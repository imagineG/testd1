package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspDtl;

/**
 * BaseTdspMnDspDtlDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TDSP_MN_DSP_DTL].
 *
 * <pre>
 *  Do not modify this file
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspMnDspDtlDaoTrx extends BaseDao {

	/**
	 * @param tdspMnDspDtl TdspMnDspDtl
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TdspMnDspDtl tdspMnDspDtl) {
		boolean result = true;
		if (tdspMnDspDtl == null) {
			result = false;
		}

		if (
			tdspMnDspDtl.getObj_mn_dsp_dtl_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * @param tdspMnDspDtl TdspMnDspDtl
	 * @throws Exception
	 */
	public  int insert(TdspMnDspDtl tdspMnDspDtl) throws Exception {
		return getSqlSession().insert("TdspMnDspDtlTrx.insert", tdspMnDspDtl);
	}

	/**
	 * @param tdspMnDspDtls TdspMnDspDtl[]
	 * @throws Exception
	 */
	public int insertMany(TdspMnDspDtl[] tdspMnDspDtls) throws Exception {
		int count = 0;
		if (tdspMnDspDtls != null) {
			for (TdspMnDspDtl tdspMnDspDtl : tdspMnDspDtls) {
				count += insert(tdspMnDspDtl);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnDspDtl TdspMnDspDtl
	 * @throws Exception
	 */
	public  int update(TdspMnDspDtl tdspMnDspDtl) throws Exception {
		if(!isPrimaryKeyValid(tdspMnDspDtl)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnDspDtl can't be null or Primary key is not set");
		}
		return getSqlSession().update("TdspMnDspDtlTrx.update", tdspMnDspDtl);
	}

	/**
	 * @param tdspMnDspDtls TdspMnDspDtl[]
	 * @throws Exception
	 */
	public  int updateMany(TdspMnDspDtl[] tdspMnDspDtls) throws Exception {
		 int count = 0;
		if (tdspMnDspDtls != null) {
			for (TdspMnDspDtl tdspMnDspDtl : tdspMnDspDtls) {
				count += update(tdspMnDspDtl);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnDspDtl TdspMnDspDtl
	 * @throws Exception
	 */
	public  int delete(TdspMnDspDtl tdspMnDspDtl)throws Exception {
		if(!isPrimaryKeyValid(tdspMnDspDtl)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnDspDtl can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TdspMnDspDtlTrx.delete", tdspMnDspDtl);
	}

	/**
	 * @param tdspMnDspDtls TdspMnDspDtl[]
	 * @throws Exception
	 */
	public int deleteMany( TdspMnDspDtl[] tdspMnDspDtls) throws Exception {
		int count = 0;
		if (tdspMnDspDtls != null) {
			for (TdspMnDspDtl tdspMnDspDtl : tdspMnDspDtls) {
				count += delete(tdspMnDspDtl);
			}
		}
		return count;
	}

}