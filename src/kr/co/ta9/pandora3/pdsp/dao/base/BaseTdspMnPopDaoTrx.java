package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnPop;

/**
 * BaseTdspMnPopDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TDSP_MN_POP].
 *
 * <pre>
 *  Do not modify this file
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 15
 */
@Repository
public class BaseTdspMnPopDaoTrx extends BaseDao {

	/**
	 * @param tdspMnPop TdspMnPop
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TdspMnPop tdspMnPop) {
		boolean result = true;
		if (tdspMnPop == null) {
			result = false;
		}
 
		if (
			tdspMnPop.getObj_mn_pop_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * @param tdspMnPop TdspMnPop
	 * @throws Exception
	 */
	public  int insert(TdspMnPop tdspMnPop) throws Exception {
		return getSqlSession().insert("TdspMnPopTrx.insert", tdspMnPop);
	}

	/**
	 * @param tdspMnPops TdspMnPop[]
	 * @throws Exception
	 */
	public int insertMany(TdspMnPop[] tdspMnPops) throws Exception {
		int count = 0;
		if (tdspMnPops != null) {
			for (TdspMnPop tdspMnPop : tdspMnPops) {
				count += insert(tdspMnPop);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnPop TdspMnPop
	 * @throws Exception
	 */
	public  int update(TdspMnPop tdspMnPop) throws Exception {
		if(!isPrimaryKeyValid(tdspMnPop)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnPop can't be null or Primary key is not set");
		}
		return getSqlSession().update("TdspMnPopTrx.update", tdspMnPop);
	}

	/**
	 * @param tdspMnPops TdspMnPop[]
	 * @throws Exception
	 */
	public  int updateMany(TdspMnPop[] tdspMnPops) throws Exception {
		 int count = 0;
		if (tdspMnPops != null) {
			for (TdspMnPop tdspMnPop : tdspMnPops) {
				count += update(tdspMnPop);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnPop TdspMnPop
	 * @throws Exception
	 */
	public  int delete(TdspMnPop tdspMnPop)throws Exception {
		if(!isPrimaryKeyValid(tdspMnPop)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnPop can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TdspMnPopTrx.delete", tdspMnPop);
	}

	/**
	 * @param tdspMnPops TdspMnPop[]
	 * @throws Exception
	 */
	public int deleteMany( TdspMnPop[] tdspMnPops) throws Exception {
		int count = 0;
		if (tdspMnPops != null) {
			for (TdspMnPop tdspMnPop : tdspMnPops) {
				count += delete(tdspMnPop);
			}
		}
		return count;
	}

}