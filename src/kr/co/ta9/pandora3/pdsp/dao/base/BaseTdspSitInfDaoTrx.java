package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TdspSitInf;

/**
 * BaseTdspSitInfDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TDSP_SIT_INF].
 *
 * <pre>
 *  Do not modify this file
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspSitInfDaoTrx extends BaseDao {

	/**
	 * @param tdspSitInf TdspSitInf
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TdspSitInf tdspSitInf) {
		boolean result = true;
		if (tdspSitInf == null) {
			result = false;
		}

		if (
			tdspSitInf.getObj_sit_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * @param tdspSitInf TdspSitInf
	 * @throws Exception
	 */
	public  int insert(TdspSitInf tdspSitInf) throws Exception {
		return getSqlSession().insert("TdspSitInfTrx.insert", tdspSitInf);
	}

	/**
	 * @param tdspSitInfs TdspSitInf[]
	 * @throws Exception
	 */
	public int insertMany(TdspSitInf[] tdspSitInfs) throws Exception {
		int count = 0;
		if (tdspSitInfs != null) {
			for (TdspSitInf tdspSitInf : tdspSitInfs) {
				count += insert(tdspSitInf);
			}
		}
		return count;		
	}

	/**
	 * @param tdspSitInf TdspSitInf
	 * @throws Exception
	 */
	public  int update(TdspSitInf tdspSitInf) throws Exception {
		if(!isPrimaryKeyValid(tdspSitInf)) {
			throw new PrimaryKeyNotSettedException("instance of TdspSitInf can't be null or Primary key is not set");
		}
		return getSqlSession().update("TdspSitInfTrx.update", tdspSitInf);
	}

	/**
	 * @param tdspSitInfs TdspSitInf[]
	 * @throws Exception
	 */
	public  int updateMany(TdspSitInf[] tdspSitInfs) throws Exception {
		 int count = 0;
		if (tdspSitInfs != null) {
			for (TdspSitInf tdspSitInf : tdspSitInfs) {
				count += update(tdspSitInf);
			}
		}
		return count;		
	}

	/**
	 * @param tdspSitInf TdspSitInf
	 * @throws Exception
	 */
	public  int delete(TdspSitInf tdspSitInf)throws Exception {
		if(!isPrimaryKeyValid(tdspSitInf)) {
			throw new PrimaryKeyNotSettedException("instance of TdspSitInf can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TdspSitInfTrx.delete", tdspSitInf);
	}

	/**
	 * @param tdspSitInfs TdspSitInf[]
	 * @throws Exception
	 */
	public int deleteMany( TdspSitInf[] tdspSitInfs) throws Exception {
		int count = 0;
		if (tdspSitInfs != null) {
			for (TdspSitInf tdspSitInf : tdspSitInfs) {
				count += delete(tdspSitInf);
			}
		}
		return count;		
	}

}