package kr.co.ta9.pandora3.pdsp.dao.base;

import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspMst;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;

/**
 * BaseTdspMnDspMstDao - BASE DAO(Base Data Access Object) class for table [TDSP_MN_DSP_MST].
 *
 * <pre>
 *   Do not modify this file
 *   Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspMnDspMstDao extends BaseDao {
	
	/**
	 * @param  parameterMap
	 * @return TdspMnDspMst
	 * @throws Exception
	 */
	public TdspMnDspMst getTdspMnDspMst(ParameterMap parameterMap) throws Exception {
		return (TdspMnDspMst) getSqlSession().selectOne("TdspMnDspMst.select", parameterMap);
	}
	
	/**
	 * @param  parameterMap
	 * @return java.util.Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map getTdspMnDspMstMap(ParameterMap parameterMap) throws Exception {
		return (Map) getSqlSession().selectOne("TdspMnDspMst.selectMap", parameterMap);
	}	

}