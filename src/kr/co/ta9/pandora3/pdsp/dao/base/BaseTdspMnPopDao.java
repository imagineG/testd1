package kr.co.ta9.pandora3.pdsp.dao.base;

import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnPop;

/**
 * BaseTdspMnPopDao - BASE DAO(Base Data Access Object) class for table [TDSP_MN_POP].
 *
 * <pre>
 *   Do not modify this file
 *   Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 15
 */
@Repository
public class BaseTdspMnPopDao extends BaseDao {
	
	/**
	 * @param  parameterMap
	 * @return TdspMnPop
	 * @throws Exception
	 */
	public TdspMnPop getTdspMnPop(ParameterMap parameterMap) throws Exception {
		return (TdspMnPop) getSqlSession().selectOne("TdspMnPop.select", parameterMap);
	}
	
	/**
	 * @param  parameterMap
	 * @return java.util.Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map getTdspMnPopMap(ParameterMap parameterMap) throws Exception {
		return (Map) getSqlSession().selectOne("TdspMnPop.selectMap", parameterMap);
	}	

}