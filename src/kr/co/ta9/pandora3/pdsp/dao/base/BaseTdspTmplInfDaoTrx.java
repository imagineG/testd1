package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TdspTmplInf;

/**
 * BaseTdspTmplInfDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TDSP_TMPL_INF].
 *
 * <pre>
 * 1. 패키지명 : kr.co.ta9.pandora3.pdsp.dao.base
 * 2. 타입명    : class
 * 3. 작성일    : 2017-11-22
 * 5. 설명       : 템플릿 기본 DaoTrx(등록/수정/삭제 쿼리 호출)
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspTmplInfDaoTrx extends BaseDao {

	/**
	 * 기본키 값 체크
	 * @param tdspTmplInf TdspTmplInf
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TdspTmplInf tdspTmplInf) {
		boolean result = true;
		if (tdspTmplInf == null) {
			result = false;
		}

		if (
			tdspTmplInf.getObj_tmpl_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * 템플릿 등록 (단건)
	 * @param tdspTmplInf TdspTmplInf
	 * @throws Exception
	 */
	public  int insert(TdspTmplInf tdspTmplInf) throws Exception {
		return getSqlSession().insert("TdspTmplInfTrx.insert", tdspTmplInf);
	}

	/**
	 * 템플릿 등록 (GRID 복수 등록) 
	 * @param tdspTmplInfs TdspTmplInf[]
	 * @throws Exception
	 */
	public int insertMany(TdspTmplInf[] tdspTmplInfs) throws Exception {
		int count = 0;
		if (tdspTmplInfs != null) {
			for (TdspTmplInf tdspTmplInf : tdspTmplInfs) {
				count += insert(tdspTmplInf);
			}
		}
		return count;		
	}

	/**
	 * 템플릿 수정 (단건)
	 * @param tdspTmplInf TdspTmplInf
	 * @throws Exception
	 */
	public  int update(TdspTmplInf tdspTmplInf) throws Exception {
		if(!isPrimaryKeyValid(tdspTmplInf)) {
			throw new PrimaryKeyNotSettedException("instance of TdspTmplInf can't be null or Primary key is not set");
		}
		return getSqlSession().update("TdspTmplInfTrx.update", tdspTmplInf);
	}

	/**
	 * 템플릿 수정 (GRID 복수 수정)
	 * @param tdspTmplInfs TdspTmplInf[]
	 * @throws Exception
	 */
	public  int updateMany(TdspTmplInf[] tdspTmplInfs) throws Exception {
		 int count = 0;
		if (tdspTmplInfs != null) {
			for (TdspTmplInf tdspTmplInf : tdspTmplInfs) {
				count += update(tdspTmplInf);
			}
		}
		return count;		
	}

	/**
	 * 템플릿 삭제 (단건)
	 * @param tdspTmplInf TdspTmplInf
	 * @throws Exception
	 */
	public  int delete(TdspTmplInf tdspTmplInf)throws Exception {
		if(!isPrimaryKeyValid(tdspTmplInf)) {
			throw new PrimaryKeyNotSettedException("instance of TdspTmplInf can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TdspTmplInfTrx.delete", tdspTmplInf);
	}

	/**
	 * 템플릿 삭제 (GRID 복수 삭제)
	 * @param tdspTmplInfs TdspTmplInf[]
	 * @throws Exception
	 */
	public int deleteMany( TdspTmplInf[] tdspTmplInfs) throws Exception {
		int count = 0;
		if (tdspTmplInfs != null) {
			for (TdspTmplInf tdspTmplInf : tdspTmplInfs) {
				count += delete(tdspTmplInf);
			}
		}
		return count;		
	}

}