package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.common.dto.DataMap;
import kr.co.ta9.pandora3.pcommon.dto.TdspTmplInf;

/**
 * BaseTdspTmplInfDao - BASE DAO(Base Data Access Object) class for table [TDSP_TMPL_INF].
 *
 * <pre>
 * 1. 패키지명 : kr.co.ta9.pandora3.pdsp.dao.base
 * 2. 타입명    : class
 * 3. 작성일    : 2017-11-22
 * 5. 설명       : 템플릿 기본 Dao(조회 쿼리 호출)
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspTmplInfDao extends BaseDao {

	/**
	 * 템플릿조회 (Object)
	 * @param  parameterMap
	 * @return TdspTmplInf
	 * @throws Exception
	 */
	public TdspTmplInf getTdspTmplInf(ParameterMap parameterMap) throws Exception {
		return (TdspTmplInf) getSqlSession().selectOne("TdspTmplInf.select", parameterMap);
	}

	/**
	 * 템플릿조회 (Map)
	 * @param  parameterMap
	 * @return java.util.Map
	 * @throws Exception
	 */
	public DataMap getTdspTmplInfMap(ParameterMap parameterMap) throws Exception {
		return getSqlSession().selectOne("TdspTmplInf.selectMap", parameterMap);
	}

}