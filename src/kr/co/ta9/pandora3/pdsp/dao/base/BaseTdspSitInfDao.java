package kr.co.ta9.pandora3.pdsp.dao.base;

import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.pcommon.dto.TdspSitInf;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;

/**
 * BaseTdspSitInfDao - BASE DAO(Base Data Access Object) class for table [TDSP_SIT_INF].
 *
 * <pre>
 *   Do not modify this file
 *   Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspSitInfDao extends BaseDao {
	
	/**
	 * @param  parameterMap
	 * @return TdspSitInf
	 * @throws Exception
	 */
	public TdspSitInf getTdspSitInf(ParameterMap parameterMap) throws Exception {
		return (TdspSitInf) getSqlSession().selectOne("TdspSitInf.select", parameterMap);
	}
	
	/**
	 * @param  parameterMap
	 * @return java.util.Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map getTdspSitInfMap(ParameterMap parameterMap) throws Exception {
		return (Map) getSqlSession().selectOne("TdspSitInf.selectMap", parameterMap);
	}	

}