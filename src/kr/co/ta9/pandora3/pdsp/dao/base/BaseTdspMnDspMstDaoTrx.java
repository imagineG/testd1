package kr.co.ta9.pandora3.pdsp.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspMst;

/**
 * BaseTdspMnDspMstDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TDSP_MN_DSP_MST].
 *
 * <pre>
 *  Do not modify this file
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspMnDspMstDaoTrx extends BaseDao {

	/**
	 * @param tdspMnDspMst TdspMnDspMst
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TdspMnDspMst tdspMnDspMst) {
		boolean result = true;
		if (tdspMnDspMst == null) {
			result = false;
		}

		if (
			tdspMnDspMst.getObj_mn_dsp_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * @param tdspMnDspMst TdspMnDspMst
	 * @throws Exception
	 */
	public  int insert(TdspMnDspMst tdspMnDspMst) throws Exception {
		return getSqlSession().insert("TdspMnDspMstTrx.insert", tdspMnDspMst);
	}

	/**
	 * @param tdspMnDspMsts TdspMnDspMst[]
	 * @throws Exception
	 */
	public int insertMany(TdspMnDspMst[] tdspMnDspMsts) throws Exception {
		int count = 0;
		if (tdspMnDspMsts != null) {
			for (TdspMnDspMst tdspMnDspMst : tdspMnDspMsts) {
				count += insert(tdspMnDspMst);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnDspMst TdspMnDspMst
	 * @throws Exception
	 */
	public  int update(TdspMnDspMst tdspMnDspMst) throws Exception {
		if(!isPrimaryKeyValid(tdspMnDspMst)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnDspMst can't be null or Primary key is not set");
		}
		return getSqlSession().update("TdspMnDspMstTrx.update", tdspMnDspMst);
	}

	/**
	 * @param tdspMnDspMsts TdspMnDspMst[]
	 * @throws Exception
	 */
	public  int updateMany(TdspMnDspMst[] tdspMnDspMsts) throws Exception {
		 int count = 0;
		if (tdspMnDspMsts != null) {
			for (TdspMnDspMst tdspMnDspMst : tdspMnDspMsts) {
				count += update(tdspMnDspMst);
			}
		}
		return count;
	}

	/**
	 * @param tdspMnDspMst TdspMnDspMst
	 * @throws Exception
	 */
	public  int delete(TdspMnDspMst tdspMnDspMst)throws Exception {
		if(!isPrimaryKeyValid(tdspMnDspMst)) {
			throw new PrimaryKeyNotSettedException("instance of TdspMnDspMst can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TdspMnDspMstTrx.delete", tdspMnDspMst);
	}

	/**
	 * @param tdspMnDspMsts TdspMnDspMst[]
	 * @throws Exception
	 */
	public int deleteMany( TdspMnDspMst[] tdspMnDspMsts) throws Exception {
		int count = 0;
		if (tdspMnDspMsts != null) {
			for (TdspMnDspMst tdspMnDspMst : tdspMnDspMsts) {
				count += delete(tdspMnDspMst);
			}
		}
		return count;
	}

}