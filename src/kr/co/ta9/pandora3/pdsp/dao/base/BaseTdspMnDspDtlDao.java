package kr.co.ta9.pandora3.pdsp.dao.base;

import java.util.Map;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspDtl;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;

/**
 * BaseTdspMnDspDtlDao - BASE DAO(Base Data Access Object) class for table [TDSP_MN_DSP_DTL].
 *
 * <pre>
 *   Do not modify this file
 *   Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTdspMnDspDtlDao extends BaseDao {
	
	/**
	 * @param  parameterMap
	 * @return TdspMnDspDtl
	 * @throws Exception
	 */
	public TdspMnDspDtl getTdspMnDspDtl(ParameterMap parameterMap) throws Exception {
		return (TdspMnDspDtl) getSqlSession().selectOne("TdspMnDspDtl.select", parameterMap);
	}
	
	/**
	 * @param  parameterMap
	 * @return java.util.Map
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public Map getTdspMnDspDtlMap(ParameterMap parameterMap) throws Exception {
		return (Map) getSqlSession().selectOne("TdspMnDspDtl.selectMap", parameterMap);
	}	

}