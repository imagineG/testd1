package kr.co.ta9.pandora3.pdsp.dao;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.pdsp.dao.base.BaseTdspMnPopDaoTrx;

/**
 * TdspMnPopDaoTrx - Transactional DAO(Data Access Object) class for table
 * [TDSP_MN_POP].
 *
 * <pre>
 *  Generated by CodeProcessor. You can freely modify this generated file.
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 15
 */
@Repository
public class TdspMnPopDaoTrx extends BaseTdspMnPopDaoTrx {

}