package kr.co.ta9.pandora3.pdsp.dao;

import org.json.simple.JSONObject;
import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.pdsp.dao.base.BaseTdspMnDspMstDao;

/**
 * TdspMnDspMstDao - DAO(Data Access Object) class for table [TDSP_MN_DSP_MST].
 *
 * <pre>
 *   Generated by CodeProcessor. You can freely modify this generated file.
 *   Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class TdspMnDspMstDao extends BaseTdspMnDspMstDao {
	/**
	 * 메인전시목록(그리드형)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public JSONObject selectTdspMnDspMstGridList(ParameterMap parameterMap) throws Exception {
		return queryForGrid("TdspMnDspMst.selectTdspMnDspMstGridList", parameterMap);
	}

}