package kr.co.ta9.pandora3.pdsp.dao;

import java.util.HashMap;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.pcommon.dto.TdspTmplInf;
import kr.co.ta9.pandora3.pdsp.dao.base.BaseTdspTmplInfDaoTrx;

/**
 * TdspTmplInfDaoTrx - Transactional DAO(Data Access Object) class for table
 * [TDSP_TMPL_INF].
 *
 * <pre>
 * 1. 패키지명 : kr.co.ta9.pandora3.pdsp.dao
 * 2. 타입명    : class
 * 3. 작성일    : 2017-11-22
 * 4. 설명       : 템플릿 daoTrx(등록/수정/삭제 쿼리 호출)
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class TdspTmplInfDaoTrx extends BaseTdspTmplInfDaoTrx {

	/**
	 * 템플릿 매핑 초기화 
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public int updateTdspTmplInfMpn(HashMap<String, Object> param) throws Exception {
		return getSqlSession().update("TdspTmplInfTrx.updateTdspTmplInfMpn", param);
	}

	/**
	 * Front 메뉴 등록
	 * @param template
	 * @return
	 * @throws Exception
	 */
	public int insertTdspTmplInfCpy(TdspTmplInf tdspTmplInf) throws Exception {
		return getSqlSession().insert("TdspTmplInfTrx.insertTdspTmplInfCpy", tdspTmplInf);
	}
	
	/**
	 * 템플릿 매핑 초기화2(템플릿 변경 전 나머지 리셋 )
	 * @param param
	 * @return
	 * @throws Exception
	 */
	public int updateTdspTmplInfMpnMnu(HashMap<String, Object> param) throws Exception {
		return getSqlSession().update("TdspTmplInfTrx.updateTdspTmplInfMpnMnu", param);
	}
}