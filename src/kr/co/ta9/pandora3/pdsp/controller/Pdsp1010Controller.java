package kr.co.ta9.pandora3.pdsp.controller;

import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.app.servlet.controller.CommonActionController;
import kr.co.ta9.pandora3.common.conf.Const;
import kr.co.ta9.pandora3.common.util.ResponseUtil;
import kr.co.ta9.pandora3.pdsp.manager.Pdsp1009Mgr;
import kr.co.ta9.pandora3.pdsp.manager.Pdsp1010Mgr;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.RequestMapping;
/**
* <pre>
* 1. 클래스명 : Pdsp1001Controller
* 2. 설명: 템플릿통합조회
* 3. 작성일 : 2018-03-29
* 4.작성자   : TANINE
* </pre>
*/

@Controller
public class Pdsp1010Controller extends CommonActionController{
	@Autowired
	private Pdsp1010Mgr pdsp1010Mgr;

	@Autowired
	private Pdsp1009Mgr pdsp1009Mgr;
	/**
	 * 메인관리 > 메인전시등록 - 시퀀스 취득(BO)
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	@RequestMapping(value="/pdsp/insertPdsp1010")
	public void insertPdsp1010(HttpServletRequest request, HttpServletResponse response) throws Exception {
		ParameterMap parameterMap = getParameterMap(request, response);
		String result = Const.BOOLEAN_SUCCESS;
		JSONObject json = new JSONObject();
		try {
			// 메인 전시 시퀀스 등록
			int mn_dsp_seq = pdsp1010Mgr.insertTdspMnDspMstSeq(parameterMap);
			
			if(mn_dsp_seq > 0) {
				 parameterMap.put("mn_dsp_seq", mn_dsp_seq);
				 pdsp1009Mgr.saveTdspMnDspDtl(parameterMap);
			} else {
				result = Const.BOOLEAN_FAIL;
			}
		}catch(Exception e) {
			e.printStackTrace();
			result = Const.BOOLEAN_FAIL;
			json.put("errorMsg", e.getMessage());
		}
		json.put("result", result);
//		json.put("RESULT", result);
		ResponseUtil.write(response, json.toJSONString());
	}


}
