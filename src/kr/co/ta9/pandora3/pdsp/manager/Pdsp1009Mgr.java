package kr.co.ta9.pandora3.pdsp.manager;

import java.io.File;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.StringUtils;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.common.conf.Config;
import kr.co.ta9.pandora3.common.exception.ConfigurationException;
import kr.co.ta9.pandora3.common.servlet.upload.UploadFile;
import kr.co.ta9.pandora3.common.util.DateUtil;
import kr.co.ta9.pandora3.common.util.FileUtil;
import kr.co.ta9.pandora3.pbbs.dao.TbbsModlInfDao;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspDtl;
import kr.co.ta9.pandora3.pcommon.dto.TdspTmplInf;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspDtlDao;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspDtlDaoTrx;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspMstDaoTrx;
import kr.co.ta9.pandora3.pdsp.dao.TdspTmplInfDao;
/**
* <pre>
* 1. 클래스명 : Pdsp1009Mgr
* 2. 설명: 메인전시상세보기
* 3. 작성일 : 2018-03-29
* 4.작성자   : TANINE
* </pre>
*/
@Service
public class Pdsp1009Mgr {
	@Autowired
	private TdspMnDspMstDaoTrx tdspMnDspMstDaoTrx;
	@Autowired
	private TdspMnDspDtlDao tdspMnDspDtlDao;
	@Autowired
	private TdspMnDspDtlDaoTrx tdspMnDspDtlDaoTrx;
	@Autowired
	private TbbsModlInfDao tbbsModlInfDao;
	@Autowired
	private TdspTmplInfDao tdspTmplInfDao;
	@Autowired
	private Pdsp1004Mgr pdsp1004Mgr;

	/**
	 * 메인전시등록/수정
	 * @param parameterMap
	 * @throws Exception
	 */
	public void saveTdspMnDspDtl(ParameterMap parameterMap) throws Exception {
		// 이미지 파일명 생성 구분자
		final String nameSeparator = "_";
		// 이미지 상대경로 구분자
		final String pathSeparator = "/";

		String folderName = "mainDisplay";	// 이미지 저장 폴더명
		String suffix     = "_no";		// 파라미터 Suffix
		int dispCnt       = 7;			// 메인 전시 컨텐츠 갯수
		int mn_dsp_seq    = Integer.parseInt(parameterMap.getValue("mn_dsp_seq"));
		String chg_flag   = parameterMap.getValue("chg_flag");	// 등록:INSERT|수정:UPDATE

		for(int i=1; i <= dispCnt; i++) {
			// 전시 타입 관련 기본변수 선언
			String dsp_tp_cd = "";	// 전시타입
			int typeCnt      = 1;	// 전시개수(Default 1)

			// 전시 타입 설정
			if(i < 5) dsp_tp_cd = "0"+i;
			else      dsp_tp_cd = "04";

			UploadFile[] uploadFiles = parameterMap.getUploadFiles("img_files"+suffix+i);	// 이미지 파일 취득
			String[] chgNameFileArr  = null;	// 변경된 파일명 배열
			String orgFilePath       = null;

			// 취득된 이미지가 있으면 이미지 수를 전시개수로
			if(uploadFiles != null ) {
				typeCnt = uploadFiles.length;
				chgNameFileArr = new String[typeCnt];
				chgNameFileArr = uploadImageFiles(uploadFiles, File.separator + folderName, nameSeparator, chgNameFileArr);
			}

			// 전시 메인 정보 등록
			for(int j=1; j<=typeCnt; j++) {

				TdspMnDspDtl tdspMnDspDtl = new TdspMnDspDtl();

				// 전시순서
				String srt_sqn = (StringUtils.isNullOrEmpty(parameterMap.getValue("srt_sqn"+suffix+i))) ? String.valueOf(j) : parameterMap.getValue("srt_sqn"+suffix+i);

				if(chgNameFileArr != null && j-1 < chgNameFileArr.length) {
					tdspMnDspDtl.setBkg_img_nm(uploadFiles[j-1].getOriginalFilename());
					if(!StringUtils.isNullOrEmpty(chgNameFileArr[j-1])) tdspMnDspDtl.setBkg_img_pth(Config.get("app.pandora3front.file.path") + folderName + pathSeparator + chgNameFileArr[j-1]);
				}

				tdspMnDspDtl.setMn_dsp_seq(mn_dsp_seq);	                                               // 메인전시순번
				tdspMnDspDtl.setDsp_tp_cd(dsp_tp_cd);                                                  // 전신유형코드
				tdspMnDspDtl.setSrt_sqn(Integer.parseInt(srt_sqn));	                                   // 전시순서
				tdspMnDspDtl.setMpn_url(parameterMap.getValue("mpn_url"+j+suffix+i));                  // 맵핑url
				tdspMnDspDtl.setTl_txt(StringUtils.isNullOrEmpty(parameterMap.getValue("tl_txt"+suffix+i)) ? null : parameterMap.getValue("tl_txt"+suffix+i));	// 제목텍스트
				tdspMnDspDtl.setStlt_txt(parameterMap.getValue("stlt_txt"+suffix+i));                  // 부제목텍스트
				tdspMnDspDtl.setBkg_colr_1(parameterMap.getValue("bkg_colr1"+suffix+i));               // 배경색상1
				tdspMnDspDtl.setBkg_colr_2(parameterMap.getValue("bkg_colr2"+suffix+i));               // 배경색상2
				tdspMnDspDtl.setTxt_colr(parameterMap.getValue("txt_colr"+suffix+i));                  // 텍스트색상
				tdspMnDspDtl.setCrtr_id(parameterMap.getValue("crtr_id"));                             // 등록자ID
				tdspMnDspDtl.setUpdr_id(parameterMap.getValue("updr_id"));                             // 수정자ID

				if(!parameterMap.getValue("tmpl_seq"+suffix+i).isEmpty()){
					tdspMnDspDtl.setTmpl_seq(Integer.parseInt(parameterMap.getValue("tmpl_seq"+suffix+i)));// 템플릿순번
				}

				if("INSERT".equals(chg_flag)) {
					// 메인전시 상세정보 등록
					tdspMnDspDtlDaoTrx.insert(tdspMnDspDtl);
				} else {

					ParameterMap data = new ParameterMap();
					data.put("mn_dsp_seq", mn_dsp_seq);						    // 메인전시순번
					data.put("mn_dsp_nm" , parameterMap.getValue("mn_dsp_nm")); // 메인전시명
					data.put("updr_id"   , parameterMap.getValue("updr_id"));	// 수정자ID
					data.put("upd_dttm"  , parameterMap.getValue("upd_dttm"));	// 수정일시

					// 메인전시 마스터 정보 수정 (메인전시명만 수정)
					if(j==1) tdspMnDspMstDaoTrx.updateTdspMnDspMstInfo(data);

					// 메인 전시 상세 순번
					String mn_dsp_dtl_seq = StringUtils.isNullOrEmpty(parameterMap.getValue("mn_dsp_dtl_seq"+j+suffix+i)) ? parameterMap.getValue("mn_dsp_dtl_seq"+suffix+i) : parameterMap.getValue("mn_dsp_dtl_seq"+j+suffix+i);
					tdspMnDspDtl.setMn_dsp_dtl_seq(Integer.parseInt(mn_dsp_dtl_seq));

					// 메인 상세 정보 조회
					List<TdspMnDspDtl> dtlList = tdspMnDspDtlDao.selectTdspMnDspDtlList(tdspMnDspDtl);
					orgFilePath                = dtlList.get(0).getBkg_img_pth();	// 배경이미지경로

					// 메인전시 상세정보 갱신
					tdspMnDspDtlDaoTrx.update(tdspMnDspDtl);

					String save_path = Config.get("app.pandora3front.file.upload.path")+ File.separator + "mainDisplay";
					String file_path = "";
					String realFileName ="";
					if (tdspMnDspDtl.getBkg_img_pth()!=null && orgFilePath!=tdspMnDspDtl.getBkg_img_pth()) {
						realFileName  = orgFilePath.substring(orgFilePath.lastIndexOf("/")+1);
						file_path     = save_path + File.separator +realFileName;
						FileUtil.deleteFile(file_path);
					}

				}
			}
		}
	}
	/**
	 * 이미지 파일 등록(파일 서버 저장)
	 * @param uploadFiles
	 * @param targetUrl
	 * @throws Exception
	 * @throws ConfigurationException
	 */
	public String[] uploadImageFiles(UploadFile[] uploadFiles, String targetPath, String separator, String[] returnArr) throws Exception {

		// 이미지 파일 확장자 유효성 검사용 프로터티 취득
		String[] validExt = Config.get("file.upload.img.valid.ext").split("\\|");
		// 체크할 이미지 확장자 배열 생성
		int[] types = new int[validExt.length];

		for(int i=0; i<validExt.length; i++) {
			if("jpg".equalsIgnoreCase(validExt[i])) types[i] = UploadFile.IMAGE_JPG;
			else if("jpeg".equalsIgnoreCase(validExt[i])) types[i] = UploadFile.IMAGE_JPEG;
			else if("gif".equalsIgnoreCase(validExt[i]))  types[i] = UploadFile.IMAGE_GIF;
			else if("png".equalsIgnoreCase(validExt[i]))  types[i] = UploadFile.IMAGE_PNG;
			else if("bmp".equalsIgnoreCase(validExt[i]))  types[i] = UploadFile.IMAGE_BMP;
		}

		for(int i = 0; i < uploadFiles.length; i++) {

			String org_img_name = uploadFiles[i].getOriginalFilename();

			if(!StringUtils.isNullOrEmpty(org_img_name)) {
				String chg_img_name = new StringBuilder().append(DateUtil.today("yyyyMMddHHmmssSSS")).append(separator).append(org_img_name).toString();
				// 파일 업로드
				uploadFiles[i].addTypes(types);
				uploadFiles[i].transferTo(Config.get("app.pandora3front.file.upload.path") + targetPath, chg_img_name);
				// 리턴용 배열 생성(저장 파일명)
				returnArr[i] = chg_img_name;
			} else {
				returnArr[i] = "";
			}
		}

		return returnArr;
	}

	/**
	 * 일반 컨텐츠
	 * BOARD TYPE1/TYPE2 : 게시판 모듈 목록 조회
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	public JSONObject selectTbbsModlInfCommonList(ParameterMap parameterMap) throws Exception {
		return tbbsModlInfDao.selectTbbsModlInfCommonList(parameterMap);
	}

	/**
	 * 템플릿 리스트 조회 (맵핑후)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public JSONObject selectTdspTmplInfList(ParameterMap parameterMap) throws Exception {
		List<TdspTmplInf> dataList = tdspTmplInfDao.selectTdspTmplInfList(parameterMap);
		List<Map<String,Object>> mapList = pdsp1004Mgr.convertObjectToMapList(dataList);
		JSONObject json = new JSONObject();
		json.put("mapList", mapList);
		return json;
	}

	/**
	 * 메인관리 > 메인전시상세(BO)
	 * @param request
	 * @param response
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public JSONObject selectTdspMnDspDtlList(ParameterMap parameterMap) throws Exception {
		List<TdspMnDspDtl> dataList = tdspMnDspDtlDao.selectTdspMnDspDtlList(parameterMap);
		List<Map<String,Object>> mapList = pdsp1004Mgr.convertObjectToMapList(dataList);
		JSONObject json = new JSONObject();
		json.put("mapList", mapList);
		return json;
	}
}
