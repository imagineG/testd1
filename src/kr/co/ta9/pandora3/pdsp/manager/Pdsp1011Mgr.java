package kr.co.ta9.pandora3.pdsp.manager;

import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.pcommon.dto.TdspSitInf;
import kr.co.ta9.pandora3.pdsp.dao.TdspSitInfDao;
import kr.co.ta9.pandora3.pdsp.dao.TdspSitInfDaoTrx;
/**
* <pre>
* 1. 클래스명 : Pdsp1011Mgr
* 2. 설명: 사이트 조회
* 3. 작성일 : 2018-04-23
* 4. 작성자   : TANINE
* </pre>
*/
@Service
public class Pdsp1011Mgr {
	@Autowired
	private TdspSitInfDao tdspSitInfDao;
	@Autowired
	private TdspSitInfDaoTrx tdspSitInfDaoTrx;

	/**
	 * 사이트 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public JSONObject selectTdspSitInfList(ParameterMap parameterMap) throws Exception {
		return tdspSitInfDao.selectTdspSitInfList(parameterMap);
	}

	/**
	 * 사이트 등록/수정/삭제
	 * @param parameterMap
	 * @throws Exception
	 */
	public void saveTdspSitInf(ParameterMap parameterMap) throws Exception {

		List<TdspSitInf> insertList = new ArrayList<TdspSitInf>();
		List<TdspSitInf> updateList = new ArrayList<TdspSitInf>();
		List<TdspSitInf> deleteList = new ArrayList<TdspSitInf>();
		parameterMap.populates(TdspSitInf.class, insertList, updateList, deleteList);

		TdspSitInf[] insert = insertList.toArray(new TdspSitInf[insertList.size()]);
		TdspSitInf[] update = updateList.toArray(new TdspSitInf[updateList.size()]);
		TdspSitInf[] delete = deleteList.toArray(new TdspSitInf[deleteList.size()]);

		tdspSitInfDaoTrx.insertMany(insert);
		tdspSitInfDaoTrx.updateMany(update);
		tdspSitInfDaoTrx.deleteMany(delete);
	}

	/**
	 * 사이트 콤보 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public JSONObject selectTdspSitInfComboList(ParameterMap parameterMap) throws Exception {
		return tdspSitInfDao.selectTdspSitInfComboList(parameterMap);
	}
}
