package kr.co.ta9.pandora3.pdsp.manager;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspMst;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspMstDaoTrx;
/**
* <pre>
* 1. 클래스명 : Pdsp1010Mgr
* 2. 설명: 템플릿상세조회
* 3. 작성일 : 2018-03-28
* 4.작성자   : TANINE
* </pre>
*/
@Service
public class Pdsp1010Mgr {
	@Autowired
	private TdspMnDspMstDaoTrx tdspMnDspMstDaoTrx;
	
	@Autowired
	private Pdsp1009Mgr pdsp1009Mgr;
	/**
	 * 메인전시등록(시퀀스등록)
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	public int insertTdspMnDspMstSeq(ParameterMap parameterMap) throws Exception {
		TdspMnDspMst tdspMnDspMst = new TdspMnDspMst();
		tdspMnDspMst.setMn_dsp_nm(parameterMap.getValue("mn_dsp_nm"));
		tdspMnDspMst.setCrtr_id(parameterMap.getValue("crtr_id"));
		tdspMnDspMst.setUpdr_id(parameterMap.getValue("updr_id"));
		tdspMnDspMstDaoTrx.insertTdspMnDspMstSeq(tdspMnDspMst);
		System.out.println(" ==================== getMn_dsp_seq >> " + tdspMnDspMst.getMn_dsp_seq() );
		
//		if(tdspMnDspMst.getMn_dsp_seq() > 0){
//			 parameterMap.put("mn_dsp_seq", tdspMnDspMst.getMn_dsp_seq());
//			 pdsp1009Mgr.saveTdspMnDspDtl(parameterMap);
//		}
		
		return tdspMnDspMst.getMn_dsp_seq();
	}
}
