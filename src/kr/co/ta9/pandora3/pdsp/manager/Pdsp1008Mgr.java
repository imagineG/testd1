package kr.co.ta9.pandora3.pdsp.manager;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.mysql.jdbc.StringUtils;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.common.conf.Config;
import kr.co.ta9.pandora3.common.util.FileUtil;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspDtl;
import kr.co.ta9.pandora3.pcommon.dto.TdspMnDspMst;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspDtlDao;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspDtlDaoTrx;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspMstDao;
import kr.co.ta9.pandora3.pdsp.dao.TdspMnDspMstDaoTrx;
/**
* <pre>
* 1. 클래스명 : Pdsp1008Mgr
* 2. 설명: 메인전시관리
* 3. 작성일 : 2018-03-29
* 4.작성자   : TANINE
* </pre>
*/
@Service
public class Pdsp1008Mgr {
	@Autowired
	private TdspMnDspMstDao tdspMnDspMstDao;
	@Autowired
	private TdspMnDspDtlDao tdspMnDspDtlDao;
	@Autowired
	private TdspMnDspMstDaoTrx tdspMnDspMstDaoTrx;
	@Autowired
	private TdspMnDspDtlDaoTrx tdspMnDspDtlDaoTrx;

	/**
	 * 메인전시목록(그리드형)
	 * @param parameterMap
	 * @return
	 */
	public JSONObject selectTdspMnDspMstGridList(ParameterMap parameterMap) throws Exception {
		return tdspMnDspMstDao.selectTdspMnDspMstGridList(parameterMap);
	}
	/**
	 * 메인관리 > 메인전시관리 (삭제 | 전시 처리 - 그리드형)
	 * @param parameterMap
	 * @throws Exception
	 */
	public void saveTdspMnDspMst(ParameterMap parameterMap) throws Exception {

		List<TdspMnDspMst> insertList = new ArrayList<TdspMnDspMst>();
		List<TdspMnDspMst> updateList = new ArrayList<TdspMnDspMst>();
		List<TdspMnDspMst> deleteList = new ArrayList<TdspMnDspMst>();

		// 변경 대상 확인
		String mn_dsp_seq = parameterMap.getValue("mn_dsp_seq");
		String chg_flg    = parameterMap.getValue("chg_flg");
		
		if("UPDATE".equals(chg_flg)) {
			if(!StringUtils.isNullOrEmpty(mn_dsp_seq)) {
				// 메인 전시여부 업데이트 ( 이미 전시되어 있는 메인은 비전시로 변경 후 해당 메인만 전시로 즉, 메인전시는 중복 불가 )
				parameterMap.put("dsp_yn", "N");
				int dsp_n_rst = tdspMnDspMstDaoTrx.updateTdspMnDspMstDsp(parameterMap);
				if(dsp_n_rst > 0) {
					parameterMap.put("dsp_yn", "Y");
					tdspMnDspMstDaoTrx.updateTdspMnDspMstDsp(parameterMap);
				}
			}
		} else {
			// 삭제대상 추출
			parameterMap.populates(TdspMnDspMst.class, insertList, updateList, deleteList);
			TdspMnDspMst[] delete = deleteList.toArray(new TdspMnDspMst[deleteList.size()]);

			int result=0;
			if (deleteList.size() > 0){

				// 메인전시 마스터 삭제
				result = tdspMnDspMstDaoTrx.deleteMany(delete);

				if(result > 0) {
					String save_path = Config.get("app.pandora3front.file.upload.path")+ File.separator + "mainDisplay";
					String file_path = "";
					String realFileName ="";

					for(int i=0;i<deleteList.size();i++){

						ParameterMap param = new ParameterMap();
						param.put("mn_dsp_seq", deleteList.get(i).getMn_dsp_seq());

						// 메인 상세 정보 조회
						List<TdspMnDspDtl> main = tdspMnDspDtlDao.selectTdspMnDspDtlList(param);
						
						for(int j=0;j<main.size();j++){
							tdspMnDspDtlDaoTrx.delete((TdspMnDspDtl) main.get(j));
							TdspMnDspDtl tmp2 = (TdspMnDspDtl) main.get(j);
							if(tmp2.getBkg_img_pth()!=null){
								realFileName  = tmp2.getBkg_img_pth().substring(tmp2.getBkg_img_pth().lastIndexOf("/")+1);
								file_path = save_path + File.separator +realFileName;
								FileUtil.deleteFile(file_path);
							}
						}
					}
				}
				
			}

		}
	}
}
