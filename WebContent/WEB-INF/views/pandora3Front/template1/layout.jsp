<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="ko">
<head>
<title>티에이나인</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes, target-densitydpi=medium-dpi, viewport-fit=cover">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/default.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/contents.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/responsive.css" media="all and (max-width: 1310px)">
<script src="${pageContext.request.contextPath}/resources/common/js/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/ui.js"></script>
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="resources/common/css/contents.css">
	<script src="resources/common/js/html5shiv.min.js"></script>
<![endif]-->
</head>

<body>
	<div id="wrap">
		<!-- header -->
		<header id="header">
			<div class="inner_wrap">
				<h1 class="logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></h1>
				<p class="btn_gnb_open"><button type="button"><span>메뉴열기</span></button></p>
				<!-- gnb -->
				<nav id="gnb">
					<div class="gnb_wrap">
						<p class="gnb_logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></p>
						<ul class="gnb_list">
							<li><a href="#">병원안내</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원안내</strong>
										<span class="tit_eng">HOSPITAL INFORMATION</span>
										<span class="txt">진실함과 정직함이 이루는<br>기업으로써 세상을 변화합니다.<br>티에이나인을 소개합니다.</span>
										<span class="link"><a href="#">병원안내 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb1.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">인사말 <span class="eng">CORPORATE GREETINGS</span></a></li>
										<li><a href="#">의료진 소개 <span class="eng">ABOUT THE DOCTOR</span></a></li>
										<li><a href="#">시설둘러보기 <span class="eng">INTERNAL FACILITIES</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">관절클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">관절클리닉</strong>
										<span class="tit_eng">JOINT CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">관절클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb2.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">어깨 클리닉 <span class="eng">SHOULDER CLINIC</span></a></li>
										<li><a href="#">무릎 클리닉 <span class="eng">KNEE CLINIC</span></a></li>
										<li><a href="#">손 / 발 클리닉 <span class="eng">HAND FOOT CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">척추클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">척추클리닉</strong>
										<span class="tit_eng">SPINE CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">척추클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb3.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">목 클리닉 <span class="eng">NECK CLINIC</span></a></li>
										<li><a href="#">허리 클리닉 <span class="eng">WAIST CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">도수치료 클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">도수치료 클리닉</strong>
										<span class="tit_eng">FREQUENCY TREATMENT</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">도수치료 클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb4.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">도수치료 <span class="eng">FREQUENCY TREATMENT</span></a></li>
										<li><a href="#">통증 클리닉 <span class="eng">ACHE CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">병원소식</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원소식</strong>
										<span class="tit_eng">HOSPITAL NEWS</span>
										<span class="txt">시스템의 차이가 곧 결과의<br>차이입니다. 최고의 전문가와<br>20년의 역사가 건강은 지킵니다.</span>
										<span class="link"><a href="#">병원소식 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb5.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">공지사항 <span class="eng">NOTICE</span></a></li>
										<li><a href="#">이벤트 <span class="eng">EVENT</span></a></li>
										<li><a href="#">언론보도 <span class="eng">PRESS RELEASE</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">오시는길</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">오시는 길</strong>
										<span class="tit_eng">LOCATE</span>
										<span class="txt">서울특별시 강남구 대치4동 894<br>티에이나인</span>
										<span class="link"><a href="#">길 찾기 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb6.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">오시는길 <span class="eng">LOCATE</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
						<p class="btn_gnb_close"><button type="button"><span>메뉴닫기</span></button></p>
					</div>
				</nav>
				<!-- //gnb -->
			</div>
		</header>
		<!-- //header -->
		
		<!-- container -->
		<div id="container">

			<div class="sub_visual sub_visual1">
				<h2 class="category_title"><span>병원</span> 소식</h2>
				<p class="txt1">저희 병원 홈페이지에 오신것을 환영합니다.</p>
				<p class="txt2">저희는 개개인의 정확한 성장판 진단을 통한<br class="mobr"> 수준높은 시술로 최선의 성장서비스를 제공합니다.<br>온 세상 아이들이 함박 웃음 짓기를 희망합니다.</p>
			</div>

			<div class="inner_wrap">
				<p class="location">
					<a href="/" class="home"><img src="${pageContext.request.contextPath}/resources/images/icon_home.png" alt="홈"></a>
					<span class="depth">병원안내</span>
					<span class="depth">병원소식</span>
				</p>

				<section>
					<h3 class="page_title"><span>Contents</span></h3>

					<section class="contnets_section">

						<div class="img"><img src="${pageContext.request.contextPath}/resources/images/img_sample02.jpg" alt=""></div>

						<h4 class="sub_title">오십견</h4>

						<section class="cont_section">
							<h5 class="sub_title2 tit_color">오십견이란?</h5>
							<div class="sub_cont">
								<p class="txt">오십견(유착성관절낭염)이란 어깨 관절을 감싸고 있는 관절낭이 두꺼워져 어깨 관절과 달라붙은 부위에 염증과 통증이 나타나 관절의 움직임을 제한하는 질환입니다. 회전근개파열, 어깨충돌증후군과 함께 대표적인 어깨 관절질환으로 어깨가 마치 얼어버린 것처럼 굳는다고 해서 ‘동결견’이라고도 불리며, 움직임이 적고 혈액순환이 잘 되지 않는 겨울철에 많이 나타나고 야간에 통증이 심해지기도 합니다.</p>
								<p class="txt">보통 50세 전후에 많이 생긴다고 해서 오십견이라 불리지만 최근에는 삼십견, 사십견 이란 말이 생길 정도로 발병연령층이 낮아지고 있습니다.</p>
							</div>
						</section>

						<section class="cont_section">
							<h5 class="sub_title2">원인과 증상</h5>
							<div class="sub_cont">
								<ul class="txt_list">
									<li>[원인]  같은 동작이 반복되는 집안일을 많이 하는 경우</li>
									<li>[원인]  책상 앞에 오래 앉아 컴퓨터 작업을 많이 하는 경우</li>
									<li>[원인]  운동량이 부족하여 근육에 노폐물이 쌓이고 혈류량이 감소하는 경우</li>
									<li>[원인]  옆으로 누워 자는 습관으로 어깨가 자주 눌리는 경우</li>
									<li>[증상]  손을 선반 위로 뻗거나 멀리 있는 반찬을 집기 힘들다.</li>
									<li>[증상]  통증이 있다 없다 하는 것이 반복되면서 점점 더 심해지는 것 같다.</li>
									<li>[증상]  통증이 어깨 뒤에서 앞으로 팔을 타고 내려와 나중엔 손까지 아프다.</li>
								</ul>
							</div>
						</section>
					</section>

					<section class="contnets_section">

						<div class="img"><img src="${pageContext.request.contextPath}/resources/images/img_sample02.jpg" alt=""></div>

						<h4 class="sub_title">오십견</h4>

						<section class="cont_section">
							<h5 class="sub_title2 tit_color">오십견이란?</h5>
							<div class="sub_cont">
								<p class="txt">오십견(유착성관절낭염)이란 어깨 관절을 감싸고 있는 관절낭이 두꺼워져 어깨 관절과 달라붙은 부위에 염증과 통증이 나타나 관절의 움직임을 제한하는 질환입니다. 회전근개파열, 어깨충돌증후군과 함께 대표적인 어깨 관절질환으로 어깨가 마치 얼어버린 것처럼 굳는다고 해서 ‘동결견’이라고도 불리며, 움직임이 적고 혈액순환이 잘 되지 않는 겨울철에 많이 나타나고 야간에 통증이 심해지기도 합니다.</p>
								<p class="txt">보통 50세 전후에 많이 생긴다고 해서 오십견이라 불리지만 최근에는 삼십견, 사십견 이란 말이 생길 정도로 발병연령층이 낮아지고 있습니다.</p>
							</div>
						</section>

						<section class="cont_section">
							<h5 class="sub_title2">원인과 증상</h5>
							<div class="sub_cont">
								<ul class="txt_list">
									<li>[원인]  같은 동작이 반복되는 집안일을 많이 하는 경우</li>
									<li>[원인]  책상 앞에 오래 앉아 컴퓨터 작업을 많이 하는 경우</li>
									<li>[원인]  운동량이 부족하여 근육에 노폐물이 쌓이고 혈류량이 감소하는 경우</li>
									<li>[원인]  옆으로 누워 자는 습관으로 어깨가 자주 눌리는 경우</li>
									<li>[증상]  손을 선반 위로 뻗거나 멀리 있는 반찬을 집기 힘들다.</li>
									<li>[증상]  통증이 있다 없다 하는 것이 반복되면서 점점 더 심해지는 것 같다.</li>
									<li>[증상]  통증이 어깨 뒤에서 앞으로 팔을 타고 내려와 나중엔 손까지 아프다.</li>
								</ul>
							</div>
						</section>
					</section>
					
				</section>

				<hr>

				<section class="bbs_list">
					<h3 class="page_title"><span>List</span></h3>

					<fieldset class="bbs_search">
						<legend>검색어 입력</legend>
						<span class="cell_sel">
							<select title="검색 구분 선택">
								<option value="">전체</option>
								<option value="">종류</option>
								<option value="">일자</option>
							</select>
						</span>
						<span class="cell_input">
							<input type="text" class="input_txt" title="검색어 입력">
						</span>
						<span class="btn_type1"><button type="button">검색</button></span>
					</fieldset>

					<div class="list">
						<style>
							.bbs_list .sort {width:90px;}
							.bbs_list .date {width:110px;}
							.bbs_list .writer {width:110px;}
						</style>
						<ul>
							<li>
								<span class="row">
									<span class="cell sort">
										<strong class="tit">종류</strong>
										<span class="cont">
											<span class="noti">공지</span>
										</span>
									</span>
									<span class="cell subject">
										<strong class="tit">제목</strong>
										<span class="cont">
											<a href="#">
												0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.0월 0일 내부수리로 인해 휴무입니다.
											</a>
										</span>
									</span>
									<span class="cell date">
										<strong class="tit">일자</strong>
										<span class="cont">
											2018.00.00
										</span>
									</span>
									<span class="cell writer">
										<strong class="tit">작성자</strong>
										<span class="cont">
											티에이나인
										</span>
									</span>
								</span>
							</li>
							<li>
								<span class="row">
									<span class="cell sort">
										<strong class="tit">종류</strong>
										<span class="cont">
											소식
										</span>
									</span>
									<span class="cell subject">
										<strong class="tit">제목</strong>
										<span class="cont">
											<a href="#">
												원내 건강강좌 안내
											</a>
										</span>
									</span>
									<span class="cell date">
										<strong class="tit">일자</strong>
										<span class="cont">
											2018.00.00
										</span>
									</span>
									<span class="cell writer">
										<strong class="tit">작성자</strong>
										<span class="cont">
											티에이나인
										</span>
									</span>
								</span>
							</li>
						</ul>
					</div>

					<div class="btn_wrap">
						<span class="btn_left">
							<span class="btn_type1"><button type="button">선택복사</button></span>
							<span class="btn_type1"><button type="button">선택삭제</button></span>
							<span class="btn_type1"><button type="button">선택이동</button></span>
						</span>
						<span class="btn_right">
							<span class="btn_type2"><button type="button">글쓰기</button></span>
						</span>
					</div>

					<div class="pagination">
						<span class="page_nav_start"><button type="button">처음</button></span>
						<span class="page_nav_prev"><button type="button">이전</button></span>
						<span class="page_num">
							<span class="num"><strong class="active" title="현재 페이지">1</strong></span>
							<span class="num"><button type="button">2</button></span>
							<span class="num"><button type="button">3</button></span>
							<span class="num"><button type="button">4</button></span>
							<span class="num"><button type="button">5</button></span>
						</span>
						<span class="page_nav_next"><button type="button">다음</button></span>
						<span class="page_nav_end"><button type="button">끝</button></span>
					</div>

				</section>

				<hr>

				<section class="bbs_view">
					<h3 class="page_title"><span>View</span></h3>

					<section class="view">

						<h4 class="tit">
							<span class="noti">공지</span>
							<span class="subject">0월 0일 내부수리로인해 휴무입니다.0월 0일 내부수리로인해 휴무입니다.0월 0일 내부수리로인해 휴무입니다.0월 0일 내부수리로인해 휴무입니다.0월 0일 내부수리로인해 휴무입니다.0월 0일 내부수리로인해 휴무입니다.</span>
							<span class="date">2018.00.00</span>
							<span class="name">티에이나인</span>
						</h4>

						<div class="cont">
							<img src="${pageContext.request.contextPath}/resources/images/img_sample01.jpg" alt="" style="width:100%;">
							<br><br>
							누구나 살면서 한번은 통증으로 인해 고통을 겪게 됩니다. 현대인들의 생활습관때문에 남녀노소를 불문하고 통증을 경험합니다. 하지만 통증이나 치료가 되지 않는 만성 통증의 경우는 삶의 질을 저하시키게 되고, 건강상의 문제가 있다는 것을 의미합니다. 특히 척추나 관절의 통증은 발병 초기에 원인을 찾아 적절한 치료를 받아 병을 진행시키지 않는 것이 중요합니다. 저희는 정확한 진단과 최선의 치료 통하여 통증 치료를 하고 도수교정치료와 주사를 통해 척추와 관절의 건강을 도모하여 통증으로 고생하는 모든 분의 삶의 질 향상에 최선을 다하겠습니다.
						</div>

						<nav class="view_nav">
							<p class="row">
								<a href="#">
									<span class="nav">prev</span>
									<span class="txt">이전 글이 없습니다.</span>
									<span class="name"></span>
								</a>
							</p>
							<p class="row">
								<a href="#">
									<span class="nav">next</span>
									<span class="txt">코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈코끼리 소아과 홈페이지 리뉴얼 오픈</span>
									<span class="name">티에이나인</span>
								</a>
							</p>
						</nav>

					</section>

					<div class="btn_wrap">
						<span class="btn_type2"><button type="button">목록</button></span>
					</div>

				</section>

			</div>

		</div>
		<!-- //container -->

		<!-- footer -->
		<footer id="footer">
			<div class="inner_wrap">
				<div class="corp_time">
					<ul>
						<li>평일 : 오전 9시 00분 ~ 오후 9시 00분</li>
						<li>점심 : 오후 1시 00분 ~ 오후 2시 00분</li>
						<li>토요일 : 오전 9시 00분 ~ 오후 4시 00분</li>
						<li>일요일 : 오전 9시 00분 ~ 오후 1시 00분</li>
					</ul>
				</div>
				<nav class="footer_nav">
					<ul>
						<li><a href="#">병원안내</a>
							<ul>
								<li><a href="#">인사말</a></li>
								<li><a href="#">의료진 소개</a></li>
								<li><a href="#">시설 둘러보기</a></li>
							</ul>
						</li>
						<li><a href="#">관절클리닉</a>
							<ul>
								<li><a href="#">어깨클리닉</a></li>
								<li><a href="#">무릎클리닉</a></li>
								<li><a href="#">손발클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">척추클리닉</a>
							<ul>
								<li><a href="#">목클리닉</a></li>
								<li><a href="#">허리클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">도수치료 클리닉</a>
							<ul>
								<li><a href="#">도수치료</a></li>
								<li><a href="#">통증클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">병원소식</a>
							<ul>
								<li><a href="#">공지사항</a></li>
								<li><a href="#">이벤트</a></li>
								<li><a href="#">언론보도</a></li>
							</ul>
						</li>
						<li><a href="#">오시는 길</a>
							<ul>
								<li><a href="#">오시는 길</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div class="footer_bottom">
				<div class="inner_wrap">
					<nav class="footer_links">
						<ul>
							<li><a href="#">환자권리의무</a></li>
							<li><a href="#">비급여진료안내</a></li>
							<li><a href="#">개인정보취급방침</a></li>
							<li><a href="#">이메일무단수집거부</a></li>
						</ul>
					</nav>
					<p class="footer_info">
						서울특별시 강남구 대치4동 894 <span class="bar">|</span> 대표전화 000-000-0000
						<small>COPYRIGHT 2018 SEOUL HOSPITAL. ALL RIGHTS RESERVED</small>
					</p>
					<div class="footer_mark">
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark1.png" alt="보건복지부 인증마크">
							<figcaption>
								보건복지부 인증<br> 의료기관
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark2.png" alt="브랜드파워 마크">
							<figcaption>
								브랜드파워<br> 의학 연속 1위
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark3.png" alt="국가브랜드 경쟁력 지수 마크">
							<figcaption>
								국가브랜드<br> 경쟁력지수 1위
							</figcaption>
						</figure>
					</div>
				</div>
			</div>
		</footer>
		<!-- //footer -->
	</div>
</body>
</html>