<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="ko">
<head>
<title>티에이나인</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes, target-densitydpi=medium-dpi, viewport-fit=cover">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/default.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/contents.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/responsive.css" media="all and (max-width: 1310px)">
<script src="${pageContext.request.contextPath}/resources/common/js/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/ui.js"></script>
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="resources/common/css/contents.css">
	<script src="resources/common/js/html5shiv.min.js"></script>
<![endif]-->
</head>

<body>
	<div id="wrap" class="main">
		<!-- header -->
		<header id="header">
			<div class="inner_wrap">
				<h1 class="logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></h1>
				<p class="btn_gnb_open"><button type="button"><span>메뉴열기</span></button></p>
				<!-- gnb -->
				<nav id="gnb">
					<div class="gnb_wrap">
						<p class="gnb_logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></p>
						<ul class="gnb_list">
							<li><a href="#">병원안내</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원안내</strong>
										<span class="tit_eng">HOSPITAL INFORMATION</span>
										<span class="txt">진실함과 정직함이 이루는<br>기업으로써 세상을 변화합니다.<br>티에이나인을 소개합니다.</span>
										<span class="link"><a href="#">병원안내 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb1.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">인사말 <span class="eng">CORPORATE GREETINGS</span></a></li>
										<li><a href="#">의료진 소개 <span class="eng">ABOUT THE DOCTOR</span></a></li>
										<li><a href="#">시설둘러보기 <span class="eng">INTERNAL FACILITIES</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">관절클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">관절클리닉</strong>
										<span class="tit_eng">JOINT CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">관절클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb2.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">어깨 클리닉 <span class="eng">SHOULDER CLINIC</span></a></li>
										<li><a href="#">무릎 클리닉 <span class="eng">KNEE CLINIC</span></a></li>
										<li><a href="#">손 / 발 클리닉 <span class="eng">HAND FOOT CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">척추클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">척추클리닉</strong>
										<span class="tit_eng">SPINE CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">척추클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb3.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">목 클리닉 <span class="eng">NECK CLINIC</span></a></li>
										<li><a href="#">허리 클리닉 <span class="eng">WAIST CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">도수치료 클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">도수치료 클리닉</strong>
										<span class="tit_eng">FREQUENCY TREATMENT</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">도수치료 클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb4.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">도수치료 <span class="eng">FREQUENCY TREATMENT</span></a></li>
										<li><a href="#">통증 클리닉 <span class="eng">ACHE CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">병원소식</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원소식</strong>
										<span class="tit_eng">HOSPITAL NEWS</span>
										<span class="txt">시스템의 차이가 곧 결과의<br>차이입니다. 최고의 전문가와<br>20년의 역사가 건강은 지킵니다.</span>
										<span class="link"><a href="#">병원소식 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb5.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">공지사항 <span class="eng">NOTICE</span></a></li>
										<li><a href="#">이벤트 <span class="eng">EVENT</span></a></li>
										<li><a href="#">언론보도 <span class="eng">PRESS RELEASE</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">오시는길</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">오시는 길</strong>
										<span class="tit_eng">LOCATE</span>
										<span class="txt">서울특별시 강남구 대치4동 894<br>티에이나인</span>
										<span class="link"><a href="#">길 찾기 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb6.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">오시는길 <span class="eng">LOCATE</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
						<p class="btn_gnb_close"><button type="button"><span>메뉴닫기</span></button></p>
					</div>
				</nav>
				<!-- //gnb -->
			</div>
		</header>
		<!-- //header -->
		
		<!-- container -->
		<div id="container">
			<div class="main_slide_wrap">
				<div class="main_slide">
					<div class="slide_item1">
						<div class="inner_wrap">
							<p class="txt1">티에이나인은 끊임없이 진화합니다.</p>
							<p class="txt2">대한민국 유일 기술 보유!<br><b>우수 의료기관 공식인증</b></p>
							<p class="txt3">의학은 인체의 구조와 기능을 조사하여 인체의 보건,<br>질병이나 상해의 치료 및 예방에 관한 방법과<br class="mobr"> 기술을 연구하는 학문이다.<br>
								기초 의학, 임상 의학, 사회 의학 등으로 나뉠 수 있다. 현대 의학은 투약이나 수술과 같은 '임상 의학'뿐만 아니라<br>해부학이나 병리학 등의 '기초 의학', 사회적 요인에 의한<br>건강 장애에 관심을 가지는 '사회 의학' 등 다방면의 분야를 아우르고 있다.</p>
						</div>
					</div>
					<div class="slide_item1">
						<div class="inner_wrap">
							<p class="txt1">티에이나인은 끊임없이 진화합니다.</p>
							<p class="txt2">대한민국 유일 기술 보유!<br><b>우수 의료기관 공식인증</b></p>
							<p class="txt3">의학은 인체의 구조와 기능을 조사하여 인체의 보건,<br>질병이나 상해의 치료 및 예방에 관한 방법과<br class="mobr"> 기술을 연구하는 학문이다.<br>
								기초 의학, 임상 의학, 사회 의학 등으로 나뉠 수 있다. 현대 의학은 투약이나 수술과 같은 '임상 의학'뿐만 아니라<br>해부학이나 병리학 등의 '기초 의학', 사회적 요인에 의한<br>건강 장애에 관심을 가지는 '사회 의학' 등 다방면의 분야를 아우르고 있다.</p>
						</div>
					</div>
					<div class="slide_item1">
						<div class="inner_wrap">
							<p class="txt1">티에이나인은 끊임없이 진화합니다.</p>
							<p class="txt2">대한민국 유일 기술 보유!<br><b>우수 의료기관 공식인증</b></p>
							<p class="txt3">의학은 인체의 구조와 기능을 조사하여 인체의 보건,<br>질병이나 상해의 치료 및 예방에 관한 방법과<br class="mobr"> 기술을 연구하는 학문이다.<br>
								기초 의학, 임상 의학, 사회 의학 등으로 나뉠 수 있다. 현대 의학은 투약이나 수술과 같은 '임상 의학'뿐만 아니라<br>해부학이나 병리학 등의 '기초 의학', 사회적 요인에 의한<br>건강 장애에 관심을 가지는 '사회 의학' 등 다방면의 분야를 아우르고 있다.</p>
						</div>
					</div>
				</div>
				<p class="slide_nav">
					<span class="slide_prev"></span>
					<span class="slide_next"></span>
				</p>
			</div>
			<script>
				$(function(){
					$('.main_slide').bxSlider({
						prevSelector : '.main_slide_wrap .slide_prev',
						nextSelector : '.main_slide_wrap .slide_next',
						prevText: '이전 슬라이드',
						nextText: '다음 슬라이드'
					});
				});
			</script>

			<div class="main_greeting">
				<div class="inner_wrap">
					<span class="pic"><img src="${pageContext.request.contextPath}/resources/images/img_main_pic.png" alt="대표원장 이미지"></span>
					<p class="txt">
						저희는 우리 가족을 믿고 맡길 수 있는 의료진이 되겠습니다.<br>
						근육이나 뼈대 따위의 운동 기관의 기능 장애를 치료하는 의학 분야를 대표로 지역주민<br>여러분의 주치의로 언제나 친근하게 방문할 수 있는 병원이 되고자 합니다. <br>
						지속적이고 체계적인 질환의 책임감 있는 모습으로 여러분의 건강을 위해 최선을 다하겠습니다.
					</p>
					<p class="sign">
						<img src="${pageContext.request.contextPath}/resources/images/img_sign.png" alt="사인"><br>
						대표원장 홍 길 동
					</p>
				</div>
			</div>

			<div class="main_banner_wrap">
				<div class="inner_wrap">
					<ul class="list">
						<li class="banner1">
							<a href="#">
								<span class="txt">3세대 맞춤형<br>인공관절</span>
								<span class="link">자세히보기 <span class="gt">&gt;</span></span>
							</a>
						</li>
						<li class="banner2">
							<a href="#">
								<span class="txt">양방향 투포투<br>인공 내시경</span>
								<span class="link">자세히보기 <span class="gt">&gt;</span></span>
							</a>
						</li>
						<li class="banner3">
							<a href="#">
								<span class="txt">무절개<br>내시경 인대 시술</span>
								<span class="link">자세히보기 <span class="gt">&gt;</span></span>
							</a>
						</li>
						<li class="banner4">
							<a href="#">
								<span class="txt">각 질환<br>프로그램</span>
								<span class="link">자세히보기 <span class="gt">&gt;</span></span>
							</a>
						</li>
					</ul>
				</div>
			</div>

			<div class="main_facilities">
				<div class="inner_wrap">
					<p class="txt">우리 가족의 건강을 믿고<br>맡길 수 있는의료진이 되겠습니다.</p>
					<p class="link"><a href="#">시설둘러보기</a></p>
				</div>
			</div>

			<div class="main_hospital_greeting">
				<div class="inner_wrap">
					<p class="tit"><strong>HOSPITAL GREETING</strong></p>
					<p class="txt">저희 종합 센터는 우리 몸을 생각하여 척추나 관절의 통증을 초기에 원인을 찾아서<br> 비수술 통증치료법을 이용하여 정확한 진단과 최선의 선택으로 과잉진료의 위험을 원천 차단하여 고객들의 건강을 지켜드리고 있습니다. [진료과목: 통증의학과, 정형외과]</p>
					<p class="txt">도수치료를 받으시는 환자분이 주위의 시선 등에 불편하시지 않고 마음 편하게 치료를 받으실 수 있도록 개인별<br> 프라이빗 치료실을 설치하여 쾌적하고 안락한 공간에서 교정치료를 받으실 수 있도록 준비하였습니다.[진료과목: 도수치료]</p>
				</div>
			</div>

			<div class="bg_content_type1">
				<div class="inner_wrap">
					<div class="bg"><img src="${pageContext.request.contextPath}/resources/images/bg_main_cont2.jpg" alt=""></div>
					<div class="cont">
						<p class="sub_txt">Must be Changed</p>
						<p class="tit">티에이나인은 전문성을 바탕으로<br> 고객의 성공을 지원하고 있습니다.</p>
						<p class="txt">티에이나인은 고객님께 맞춰 모든 디지털 기기에 완벽하게 대응하는<br>다양한 비즈니스에서 고객의 성공을 지원합니다.</p>
						<p class="link"><a href="#">의료진안내</a></p>
					</div>
				</div>
			</div>
			<div class="bg_content_type2">
				<div class="inner_wrap">
					<div class="bg"><img src="${pageContext.request.contextPath}/resources/images/bg_main_cont3.jpg" alt=""></div>
					<div class="cont">
						<p class="sub_txt">Must be Changed</p>
						<p class="tit">새로운 기술, 비즈니스에 대한<br>끊임없는 변화에 도전합니다.</p>
						<p class="txt">티에이나인의 자존심은 고객에 대한 존중과 배려, 믿음입니다.<br>다양한 디자인 방향으로 도전하겠습니다.</p>
						<p class="link"><a href="#">언론보도 바로가기</a></p>
					</div>
				</div>
			</div>

			<section class="main_info_wrap">
				<h2 class="tit">HOSPITAL INFORMATION</h2>
				<div class="inner_wrap">
					<section class="sub_section active_section">
						<h3 class="sub_tit">공지사항</h3>
						<p class="txt">항상 정확한 상담과 정확한 시술로 환자 한 분 한분의 건강하고 아름다운 미소를 되찾아 드리겠습니다.아름다운 미소를 되찾아 드리겠습니다.</p>
						<p class="link"><a href="#">바로가기</a></p>
					</section>
					<section class="sub_section">
						<h3 class="sub_tit">티에이나인 이벤트</h3>
						<p class="txt">건강관리를 간편하게! 티에나인의 좋은 기회를 누려보세요~!</p>
						<p class="link"><a href="#">바로가기</a></p>
					</section>
					<section class="sub_section">
						<h3 class="sub_tit">오시는 길 안내</h3>
						<p class="txt">저희 종합센터에 오신것을 환영합니다. 편리함이 가득한 티에이나인입니다.</p>
						<p class="link"><a href="#">바로가기</a></p>
					</section>
				</div>
			</section>

		</div>
		<!-- //container -->

		<!-- footer -->
		<footer id="footer">
			<div class="inner_wrap">
				<div class="corp_time">
					<ul>
						<li>평일 : 오전 9시 00분 ~ 오후 9시 00분</li>
						<li>점심 : 오후 1시 00분 ~ 오후 2시 00분</li>
						<li>토요일 : 오전 9시 00분 ~ 오후 4시 00분</li>
						<li>일요일 : 오전 9시 00분 ~ 오후 1시 00분</li>
					</ul>
				</div>
				<nav class="footer_nav">
					<ul>
						<li><a href="#">병원안내</a>
							<ul>
								<li><a href="#">인사말</a></li>
								<li><a href="#">의료진 소개</a></li>
								<li><a href="#">시설 둘러보기</a></li>
							</ul>
						</li>
						<li><a href="#">관절클리닉</a>
							<ul>
								<li><a href="#">어깨클리닉</a></li>
								<li><a href="#">무릎클리닉</a></li>
								<li><a href="#">손발클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">척추클리닉</a>
							<ul>
								<li><a href="#">목클리닉</a></li>
								<li><a href="#">허리클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">도수치료 클리닉</a>
							<ul>
								<li><a href="#">도수치료</a></li>
								<li><a href="#">통증클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">병원소식</a>
							<ul>
								<li><a href="#">공지사항</a></li>
								<li><a href="#">이벤트</a></li>
								<li><a href="#">언론보도</a></li>
							</ul>
						</li>
						<li><a href="#">오시는 길</a>
							<ul>
								<li><a href="#">오시는 길</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div class="footer_bottom">
				<div class="inner_wrap">
					<nav class="footer_links">
						<ul>
							<li><a href="#">환자권리의무</a></li>
							<li><a href="#">비급여진료안내</a></li>
							<li><a href="#">개인정보취급방침</a></li>
							<li><a href="#">이메일무단수집거부</a></li>
						</ul>
					</nav>
					<p class="footer_info">
						서울특별시 강남구 대치4동 894 <span class="bar">|</span> 대표전화 000-000-0000
						<small>COPYRIGHT 2018 SEOUL HOSPITAL. ALL RIGHTS RESERVED</small>
					</p>
					<div class="footer_mark">
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark1.png" alt="보건복지부 인증마크">
							<figcaption>
								보건복지부 인증<br> 의료기관
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark2.png" alt="브랜드파워 마크">
							<figcaption>
								브랜드파워<br> 의학 연속 1위
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark3.png" alt="국가브랜드 경쟁력 지수 마크">
							<figcaption>
								국가브랜드<br> 경쟁력지수 1위
							</figcaption>
						</figure>
					</div>
				</div>
			</div>
		</footer>
		<!-- //footer -->
	</div>
</body>
</html>