<!DOCTYPE html>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<html lang="ko">
<head>
<title>티에이나인</title>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=Edge">
<meta name="format-detection" content="telephone=no">
<meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0, maximum-scale=1.0, user-scalable=yes, target-densitydpi=medium-dpi, viewport-fit=cover">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/default.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/common.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/contents.css">
<link rel="stylesheet" type="text/css" href="${pageContext.request.contextPath}/resources/common/css/responsive.css" media="all and (max-width: 1310px)">
<script src="${pageContext.request.contextPath}/resources/common/js/jquery-1.12.4.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/jquery.bxslider.min.js"></script>
<script src="${pageContext.request.contextPath}/resources/common/js/ui.js"></script>
<!--[if lt IE 9]>
	<link rel="stylesheet" type="text/css" href="resources/common/css/contents.css">
	<script src="resources/common/js/html5shiv.min.js"></script>
<![endif]-->
</head>

<body>
	<div id="wrap">
		<!-- header -->
		<header id="header">
			<div class="inner_wrap">
				<h1 class="logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></h1>
				<p class="btn_gnb_open"><button type="button"><span>메뉴열기</span></button></p>
				<!-- gnb -->
				<nav id="gnb">
					<div class="gnb_wrap">
						<p class="gnb_logo"><a href="/"><img src="${pageContext.request.contextPath}/resources/images/img_logo.png" alt="로고"></a></p>
						<ul class="gnb_list">
							<li><a href="#">병원안내</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원안내</strong>
										<span class="tit_eng">HOSPITAL INFORMATION</span>
										<span class="txt">진실함과 정직함이 이루는<br>기업으로써 세상을 변화합니다.<br>티에이나인을 소개합니다.</span>
										<span class="link"><a href="#">병원안내 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb1.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">인사말 <span class="eng">CORPORATE GREETINGS</span></a></li>
										<li><a href="#">의료진 소개 <span class="eng">ABOUT THE DOCTOR</span></a></li>
										<li><a href="#">시설둘러보기 <span class="eng">INTERNAL FACILITIES</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">관절클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">관절클리닉</strong>
										<span class="tit_eng">JOINT CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">관절클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb2.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">어깨 클리닉 <span class="eng">SHOULDER CLINIC</span></a></li>
										<li><a href="#">무릎 클리닉 <span class="eng">KNEE CLINIC</span></a></li>
										<li><a href="#">손 / 발 클리닉 <span class="eng">HAND FOOT CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">척추클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">척추클리닉</strong>
										<span class="tit_eng">SPINE CLINIC</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">척추클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb3.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">목 클리닉 <span class="eng">NECK CLINIC</span></a></li>
										<li><a href="#">허리 클리닉 <span class="eng">WAIST CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">도수치료 클리닉</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">도수치료 클리닉</strong>
										<span class="tit_eng">FREQUENCY TREATMENT</span>
										<span class="txt">관절 통증 비수술 치료!<br>체계적인 치료 프로그램!<br>풍부한 노하우와 오랜 임상경력!</span>
										<span class="link"><a href="#">도수치료 클리닉 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb4.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">도수치료 <span class="eng">FREQUENCY TREATMENT</span></a></li>
										<li><a href="#">통증 클리닉 <span class="eng">ACHE CLINIC</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">병원소식</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">병원소식</strong>
										<span class="tit_eng">HOSPITAL NEWS</span>
										<span class="txt">시스템의 차이가 곧 결과의<br>차이입니다. 최고의 전문가와<br>20년의 역사가 건강은 지킵니다.</span>
										<span class="link"><a href="#">병원소식 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb5.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">공지사항 <span class="eng">NOTICE</span></a></li>
										<li><a href="#">이벤트 <span class="eng">EVENT</span></a></li>
										<li><a href="#">언론보도 <span class="eng">PRESS RELEASE</span></a></li>
									</ul>
								</div>
							</li>
							<li><a href="#">오시는길</a>
								<div class="gnb_2depth">
									<div class="info_2depth">
										<strong class="tit">오시는 길</strong>
										<span class="tit_eng">LOCATE</span>
										<span class="txt">서울특별시 강남구 대치4동 894<br>티에이나인</span>
										<span class="link"><a href="#">길 찾기 바로가기</a></span>
										<span class="thumb"><img src="${pageContext.request.contextPath}/resources/images/img_2depth_thumb6.jpg" alt=""></span>
									</div>
									<ul class="gnb_2depth_list">
										<li><a href="#">오시는길 <span class="eng">LOCATE</span></a></li>
									</ul>
								</div>
							</li>
						</ul>
						<p class="btn_gnb_close"><button type="button"><span>메뉴닫기</span></button></p>
					</div>
				</nav>
				<!-- //gnb -->
			</div>
		</header>
		<!-- //header -->
		
		<!-- container -->
		<div id="container">

			<div class="sub_visual sub_visual1">
				<h2 class="category_title"><span>병원</span> 인사말</h2>
				<p class="txt1">저희 병원 홈페이지에 오신것을 환영합니다.</p>
				<p class="txt2">저희는 개개인의 정확한 성장판 진단을 통한<br class="mobr"> 수준높은 시술로 최선의 성장서비스를 제공합니다.<br>온 세상 아이들이 함박 웃음 짓기를 희망합니다.</p>
			</div>

			<div class="inner_wrap">
				<p class="location">
					<a href="/" class="home"><img src="${pageContext.request.contextPath}/resources/images/icon_home.png" alt="홈"></a>
					<span class="depth">병원안내</span>
					<span class="depth">병원인사말</span>
				</p>

				<section class="greetings">
					<h3 class="page_title"><span>HOSPITAL GREETINGS</span></h3>

					<section class="contnets_section medi_list">

						<div class="img"><img src="${pageContext.request.contextPath}/resources/images/img_sample02.jpg" alt=""></div>

						<h4 class="sub_title type_eng">우리 몸을 생각한 비수술 치료요법 <span class="eng">HOSPITAL OF KOREAN MEDICINE</span></h4>

						<div class="cont_section">
							<ul>
								<li class="medi1">
									<strong>미션</strong>
									사랑과 정성이 척추를 건강하게 만듭니다.<br>
									통증으로 고통 받는 고객을 내 가족처럼 사랑과 정성으로<br> 보살펴 아프기 전보다 더욱 건강한 척추가<br> 되도록 노력하겠습니다.
								</li>
								<li class="medi2">
									<strong>비전</strong>
									세계가 인정한 글로벌 척추치료 병원<br> 전통적 가치와 기술적 혁신을 바탕으로 세계적으로<br> 인정받는 글로벌 척추치료 한방병원으로 도약
								</li>
								<li class="medi3">
									<strong>인재상</strong>
									긍정적인 사고를 통해 변화를 선도하는 뜨거운 열정과<br> 따스한 마음을 지닌 실행중심 인재,<br> With Passion, Based on DOING
								</li>
								<li class="medi4">
									<strong>의료진</strong>
									최상위급 전문 도수치료사들과 간호사, 상담사들이<br> 상주하여 친절하고 체계적인 서비스를 실천하고 있습니다.<br>
									정확한 상담과 정확한 시술로 환자 한 분 한분의<br> 건강하고 아름다운 미소를 되찾아 드리겠습니다.
								</li>
							</ul>
						</div>

					</section>

					<section class="contnets_section mark_list">

						<h4 class="sub_title type_eng">국가브랜드 의료기관 인증 <span class="eng">NATIONAL BRAND MEDICAL INSTITUTION</span></h4>

						<div class="cont_section">
							<figure>
								<img src="${pageContext.request.contextPath}/resources/images/img_greetings_mark1.jpg" alt="">
								<figcaption>
									보건복지부 의료기관 인증업체
								</figcaption>
							</figure>
							<figure>
								<img src="${pageContext.request.contextPath}/resources/images/img_greetings_mark2.jpg" alt="">
								<figcaption>
									레드닷(Reddot) 디자인 어워드
								</figcaption>
							</figure>
							<figure>
								<img src="${pageContext.request.contextPath}/resources/images/img_greetings_mark3.jpg" alt="">
								<figcaption>
									NBCI 국가브랜드 경쟁력 인정
								</figcaption>
							</figure>
						</div>

					</section>

					<section class="contnets_section">

						<h4 class="sub_title type_eng">병원장 인사말 <span class="eng">GREETINGS FROM THE HOSPITAL</span></h4>

						<div class="cont_section">
							<div class="sub_cont">
								<p>60년간 비수술 척추치료의 새 역사를 열어온 병원은 국내 최초로 보건복지부 지정 척추전문병원 인증을 받으며 대한민국 대표 비수술 척추 치료 브랜드로 자리 잡았습니다. 그간 고객님의 사랑으로 발전을 이룬 병원은 2017년 신사로 확장 이전 하며 보다 업그레이드된 의료 서비스를 갖추었습니다.</p>

								<p>환자분들은 이제 여러 병원을 전전하실 필요 없이, 한 곳에서 종합적인 한방·양방 의료 서비스를 받으실 수 있습니다.<br>세계화를 이끄는 국제진료센터는 70여 개국 2만 여 명의 외국 초진 환자들을 수용하기 위해 외국인 전용 진료공간을 넓혔습니다. 또한 해외 유수의 연구기관과 학술적인 교류를 강화하고 해외 의료진을 대상으로 한의학을 전파하는 등 한의학의 세계화에 앞장설 계획입니다.</p>

								<p>앞으로도 의료진 및 임직원 모두는 척추 관절 질환만큼은 수술에서 자유로울 수 있도록 정성 어린 치료와 고객의 마음까지 살피는 의료서비스를 실천하고자 그 소임을 다할 것입니다.  감사합니다.</p>
							</div>
						</div>

					</section>
					
				</section>

			</div>

		</div>
		<!-- //container -->

		<!-- footer -->
		<footer id="footer">
			<div class="inner_wrap">
				<div class="corp_time">
					<ul>
						<li>평일 : 오전 9시 00분 ~ 오후 9시 00분</li>
						<li>점심 : 오후 1시 00분 ~ 오후 2시 00분</li>
						<li>토요일 : 오전 9시 00분 ~ 오후 4시 00분</li>
						<li>일요일 : 오전 9시 00분 ~ 오후 1시 00분</li>
					</ul>
				</div>
				<nav class="footer_nav">
					<ul>
						<li><a href="#">병원안내</a>
							<ul>
								<li><a href="#">인사말</a></li>
								<li><a href="#">의료진 소개</a></li>
								<li><a href="#">시설 둘러보기</a></li>
							</ul>
						</li>
						<li><a href="#">관절클리닉</a>
							<ul>
								<li><a href="#">어깨클리닉</a></li>
								<li><a href="#">무릎클리닉</a></li>
								<li><a href="#">손발클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">척추클리닉</a>
							<ul>
								<li><a href="#">목클리닉</a></li>
								<li><a href="#">허리클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">도수치료 클리닉</a>
							<ul>
								<li><a href="#">도수치료</a></li>
								<li><a href="#">통증클리닉</a></li>
							</ul>
						</li>
						<li><a href="#">병원소식</a>
							<ul>
								<li><a href="#">공지사항</a></li>
								<li><a href="#">이벤트</a></li>
								<li><a href="#">언론보도</a></li>
							</ul>
						</li>
						<li><a href="#">오시는 길</a>
							<ul>
								<li><a href="#">오시는 길</a></li>
							</ul>
						</li>
					</ul>
				</nav>
			</div>
			<div class="footer_bottom">
				<div class="inner_wrap">
					<nav class="footer_links">
						<ul>
							<li><a href="#">환자권리의무</a></li>
							<li><a href="#">비급여진료안내</a></li>
							<li><a href="#">개인정보취급방침</a></li>
							<li><a href="#">이메일무단수집거부</a></li>
						</ul>
					</nav>
					<p class="footer_info">
						서울특별시 강남구 대치4동 894 <span class="bar">|</span> 대표전화 000-000-0000
						<small>COPYRIGHT 2018 SEOUL HOSPITAL. ALL RIGHTS RESERVED</small>
					</p>
					<div class="footer_mark">
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark1.png" alt="보건복지부 인증마크">
							<figcaption>
								보건복지부 인증<br> 의료기관
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark2.png" alt="브랜드파워 마크">
							<figcaption>
								브랜드파워<br> 의학 연속 1위
							</figcaption>
						</figure>
						<figure>
							<img src="${pageContext.request.contextPath}/resources/images/img_foot_mark3.png" alt="국가브랜드 경쟁력 지수 마크">
							<figcaption>
								국가브랜드<br> 경쟁력지수 1위
							</figcaption>
						</figure>
					</div>
				</div>
			</div>
		</footer>
		<!-- //footer -->
	</div>
</body>
</html>