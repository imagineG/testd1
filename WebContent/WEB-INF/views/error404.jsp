<%-- 
    작성자 : HIJ
    개요 : 메인 > 관련사이트
    수정사항 :
        2017-12-04 최초작성
        2017-12-14 화면수정
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- meta File include -->
<%@ include file="/WEB-INF/views/pandora3Front/common/include/meta.jsp" %>
<script type="text/javascript">
$(document).ready(function() {
	
});
</script>
<style>
#header{position:relative;width:90%;}
#header h1{line-height:50px; padding-left:50px}
#header .menu{position:absolute;top:0;right:0}
</style>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- container -->
	<div class="contents-container refer-site">
		<div id="header">
	        <h1><a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/pandora3Front/images/logo@2x.png" alt="pandora3" /></a></h1>
	        <p class="menu"><a href="${pageContext.request.contextPath}/">판도라홈</a> | <a >판도라 고객센터</a></p>
	    </div>
		<!-- contents-main -->
			<div class="contents-main">
				<!-- category title -->
				<!--// category title -->
				<!-- tab -->
				<nav class="tab"></nav>
				<!--// tab -->
				<!-- greeting -->
				<!-- 서비스 준비중 -->
				<div class="m-t-30 preparing">
					<p>
						<img src="/pandora3/resources/pandora3Front/images/bg/preparingImg.png" alt="" />
					</p>
					<h5>죄송합니다.
					<br/>요청하신 페이지를 찾을 수 없습니다.
					</h5>
					<p>
						방문하시려는 페이지의 주소가 잘못 입력되었거나,
						<br/>페이지의 주소가 변경 혹은 삭제되어 요청하신 페이지를 찾을 수 없습니다.
						<br/><br/>입력하신 주소가 정확한지 다시 한번 확인해 주시기 바랍니다.
						<br/>관련 문의사항은 <a>판도라 고객센터</a>에 알려주시면 안내해 드리겠습니다.
						<br/>
						감사합니다.
					</p>
				</div>
				<!-- 서비스 준비중 -->
				<!--// greeting -->
			</div>
		<!--// family site -->
	</div>
	<!--// container -->

</div>
<!-- //wrapper -->
</body>
</html>