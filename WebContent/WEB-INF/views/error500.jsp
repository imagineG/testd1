<%-- 
    작성자 : HIJ
    개요 : 메인 > 관련사이트
    수정사항 :
        2017-12-04 최초작성
        2017-12-14 화면수정
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- meta File include -->
<%@ include file="/WEB-INF/views/pandora3Front/common/include/meta.jsp" %>
<script type="text/javascript">
$(document).ready(function() {
	
});
</script>
</script>
<style>
#header{position:relative;width:90%;}
#header h1{line-height:50px; padding-left:50px}
#header .menu{position:absolute;top:0;right:0}
</style>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- container -->
	<div class="contents-container refer-site">
		<div id="header">
        <h1><a href="${pageContext.request.contextPath}/"><img src="${pageContext.request.contextPath}/resources/pandora3Front/images/logo@2x.png" alt="pandora3" /></a></h1>
        <p class="menu"><a href="${pageContext.request.contextPath}/">판도라홈</a> | <a >판도라 고객센터</a></p>
    	</div>
		<!-- contents-main -->
			<div class="contents-main">
				<!-- category title -->
				<!--// category title -->
				<!-- tab -->
				<nav class="tab"></nav>
				<!--// tab -->
				<!-- greeting -->
				<!-- 서비스 준비중 -->
				<div class="m-t-30 preparing">
					<p>
						<img src="/pandora3/resources/pandora3Front/images/bg/preparingImg.png" alt="" />
					</p>
					<h5>요청 작업 처리 중 에러가 발생했습니다.</h5>
					<p>
						잠시 후 다시 시도해주세요.<br />
					</p>
				</div>
				<!-- 서비스 준비중 -->
				<!--// greeting -->
			</div>
		<!--// family site -->
	</div>
	<!--// container -->
	
</div>
<!-- //wrapper -->
</body>
</html>