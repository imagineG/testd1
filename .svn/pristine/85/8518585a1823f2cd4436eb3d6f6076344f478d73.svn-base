<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="TbbsWbznDspMst">

	<!-- 인사이트 최근 웹진아이디 추출 -->
	<select id="selectTbbsWbznDspMstWbznSeq" resultType="String">
	/* [TbbsWbznDspMst.xml][selectTbbsWbznDspMstWbznSeq][인사이트 최근 웹진아이디 추출][TANINE] */
		SELECT WBZN_SEQ
		  FROM(
		        SELECT A.WBZN_SEQ
				  FROM TBBS_WBZN_DSP_MST A
				     , TBBS_WBZN_DSP_MN_INF B
				     , TBBS_WBZN_DSP_DTL C
				 WHERE A.WBZN_SEQ = B.WBZN_SEQ
				   AND A.WBZN_SEQ = C.WBZN_SEQ
				 GROUP BY A.WBZN_SEQ
				 ORDER BY A.WBZN_SEQ DESC      
		       )
		   WHERE 1=1
		     AND ROWNUM = 1
	</select>

	<!-- 인사이트 웹진 콤보 정보 조회 -->
	<select id="selectTbbsWbznDspMstComboInf" resultType="TbbsWbznDspMst">
	/* [TbbsWbznDspMst.xml][selectTbbsWbznDspMstComboInf][인사이트 웹진 콤보 정보 조회][TANINE] */
		SELECT A.WBZN_SEQ              AS WBZN_SEQ
		     , A.TMP_SEQ               AS TMP_SEQ
		     , A.WBZN_NM               AS WBZN_NM
		     , A.Y_NO                  AS Y_NO
		     , A.M_NO                  AS M_NO
		     , A.THUMB_FPATH           AS THUMB_FPATH
		 FROM TBBS_WBZN_DSP_MST A
		    , TBBS_WBZN_DSP_MN_INF B
		    , TBBS_WBZN_DSP_DTL C
		WHERE A.WBZN_SEQ = B.WBZN_SEQ
		  AND A.WBZN_SEQ = C.WBZN_SEQ
		GROUP BY A.WBZN_SEQ, A.TMP_SEQ , A.WBZN_NM , A.Y_NO , A.M_NO , A.THUMB_FPATH
		ORDER BY A.WBZN_SEQ DESC
	</select>

	<!-- 웹진 PDF 파일 다운로드 파일 아이디 조회 -->
	<select id="selectTbbsWbznDspMstFileInfo" resultType="String">
    /* [TbbsWbznDspMst.xml][selectTbbsWbznDspMstFileInfo][웹진 PDF 파일 다운로드 파일 아이디 조회][TANINE] */
        SELECT FL_SEQ
          FROM (
                SELECT FL_SEQ
        		  FROM TBBS_FL_INF
        		 WHERE MODL_SEQ    = '6'
        		   AND UPL_TRG_SEQ = #{wbzn_seq, jdbcType=NUMERIC}
        		 ORDER BY CRT_DTTM DESC 
                )
         WHERE 1=1
           AND ROWNUM = 1
    </select>

	<!-- 웹진 마스터 그리드 조회(BO) -->
	<select id="selectTbbsWbznDspMstGridList" parameterType="ParameterMap" resultType="TbbsWbznDspMst">
	/* [TbbsWbznDspMst.xml][selectTbbsWbznDspMstGridList][웹진 마스터 그리드 조회(BO)][TANINE] */
		<include refid="Common.getPaging-Prefix" />
			SELECT WM.TMP_SEQ                           AS TMP_SEQ
			     , WM.WBZN_SEQ                          AS WBZN_SEQ
			     , WM.WBZN_NM                           AS WBZN_NM
			     , WM.Y_NO                              AS Y_NO
			     , WM.M_NO                              AS M_NO
			     , T1.CTG_ALL                           AS CTG_ALL
			     , TO_CHAR(WM.CRT_DTTM, 'YYYY-MM-DD')   AS F_CRT_DTTM
			     , (SELECT S1.LGN_ID 
			          FROM TMBR_ADM_LGN_INF S1 
			         WHERE S1.USR_ID = WM.CRTR_ID
			       )                                    AS C_LGN_ID
			     , TO_CHAR(WM.UPD_DTTM, 'YYYY-MM-DD')   AS F_UPD_DTTM
			     , (SELECT S1.LGN_ID 
			          FROM TMBR_ADM_LGN_INF S1 
			         WHERE S1.USR_ID = WM.UPDR_ID
			       )                                    AS U_LGN_ID
			     , WM.PDF_NM                            AS PDF_NM
			     , WM.PDF_FPATH                         AS PDF_FPATH
			 FROM TBBS_WBZN_DSP_MST WM
			    , (
					SELECT WBZN_SEQ
					     , LISTAGG(CTG_NM, ',') WITHIN GROUP (ORDER BY CTG_SEQ) CTG_ALL
					  FROM TBBS_WBZN_DSP_MN_INF
					 WHERE 1=1
					 GROUP BY WBZN_SEQ
				  ) T1
			 WHERE T1.WBZN_SEQ = WM.WBZN_SEQ
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(y_no)">
			   AND WM.Y_NO = #{y_no, jdbcType=VARCHAR}
			</if>
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(wbzn_nm)">  
			   AND WM.WBZN_NM LIKE ('%'|| #{wbzn_nm ,jdbcType=VARCHAR} ||'%')
			</if>
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
			 ORDER BY T1.${sidx} ${sord}
			</if>
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">
			 ORDER BY T1.WBZN_SEQ
			</if>
		<include refid="Common.getPaging-Suffix" />	
	</select>
	
	<!-- 웹진 화면조회  -->
	<!-- 2019-02-18 pandora3.1 BaseWebzineDisp.xml 3개의 웹진화면조회 쿼리 하나로 통합 -->
	<select id="selectTbbsWbznDspMstInf" resultType="TbbsWbznDspMst">
		/* [TbbsWbznDspMst.xml][selectTbbsWbznDspMstInf][웹진 화면조회][TANINE] */
		SELECT WBZN_SEQ                                                                   AS WBZN_SEQ
             , WBZN_NM                                                                    AS WBZN_NM
             , TMP_SEQ                                                                    AS TMP_SEQ
             , Y_NO                                                                       AS Y_NO
             , M_NO                                                                       AS M_NO
             , THUMB_FPATH                                                                AS THUMB_FPATH
             , SUBSTR(THUMB_FPATH, INSTR(THUMB_FPATH, '/', -1, 1)+1, LENGTH(THUMB_FPATH)) AS THUMB_NM
             , SUBSTR(PDF_FPATH, INSTR(PDF_FPATH, '/', -1, 1)+1, LENGTH(PDF_FPATH))       AS CHG_PDF_NM
             , PDF_NM                                                                     AS PDF_NM
		     , PDF_FPATH                                                                  AS PDF_FPATH
		FROM TBBS_WBZN_DSP_MST
		WHERE WBZN_SEQ = #{wbzn_seq ,jdbcType=NUMERIC}
	</select>

</mapper>