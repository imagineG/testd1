<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="TsysAdmMnu">
    <!-- 관리자 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuList" parameterType="ParameterMap" resultType="TsysAdmMnu">
       /*[TsysAdmMnu.xml][selectTsysAdmMnuList][관리자 메뉴 리스트 조회][TANINE]*/
        SELECT MNU_SEQ             -- 메뉴번호
             , UP_MNU_SEQ          -- 상위메뉴아이디
             , MNU_NM              -- 메뉴명
             , MNU_DPTH            -- 메뉴깊이
             , URL                 -- url
             , MNU_TP_CD           -- 메뉴타입코드
             , MNU_YN              -- 메뉴여부
             , US_YN               -- 사용여부
             , LST_MNU_YN          -- 마지막메뉴여부
             , SRT_SQN             -- 정렬순서(전시순서)
             , MNUL_ID             -- 메뉴얼ID
             , MNU_DESC            -- 메뉴설명
             , CRTR_ID             -- 생성자ID
             , CRT_DTTM            -- 생성일시
             , UPDR_ID             -- 수정자ID
             , UPD_DTTM            -- 수정일시
          FROM TSYS_ADM_MNU
         WHERE 1=1
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(up_mnu_seq)">AND UP_MNU_SEQ = #{up_mnu_seq  ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_tp_cd )">AND MNU_TP_CD  = #{mnu_tp_cd   ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_dpth  )">AND MNU_DPTH   = #{mnu_dpth    ,jdbcType=NUMERIC}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn     )">AND US_YN      = #{us_yn       ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_yn    )">AND MNU_YN     = #{mnu_yn      ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm    )">AND MNU_NM LIKE CONCAT('%',#{mnu_nm    ,jdbcType=VARCHAR},'%')</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url       )">AND URL        = #{url         ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(frnt_yn   )">AND FRNT_YN    = #{frnt_yn     ,jdbcType=VARCHAR}</if>
     ORDER BY UP_MNU_SEQ, MNU_NM
    </select>

    <!-- 관리자 시스템 메뉴 그리드 리스트 조회 -->
    <select id="selectTsysAdmMnuDepthGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuDepthGridList][관리자 시스템 메뉴 그리드 리스트 조회 ][TANINE]*/
             SELECT T2.*
                  , CEILING(CONVERT(FLOAT,T2.TOT_CNT) / CONVERT(FLOAT,#{rows, jdbcType=NUMERIC})) AS TOTAL_PAGE
                  , T2.TOT_CNT AS TOTAL_COUNT
                  , CEILING(CONVERT(FLOAT,T2.ROW_NUM) / CONVERT(FLOAT,#{rows, jdbcType=NUMERIC})) AS PAGE
              FROM (
                    SELECT T1.*
                         , COUNT(1) OVER() AS TOT_CNT
                         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
                         , ROW_NUMBER() OVER(ORDER BY T1.${sidx}  ${sord}) ROW_NUM
                         </if>
                         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">   
                         , ROW_NUMBER() OVER(ORDER BY T1.MNU_SEQ ) ROW_NUM
                         </if>
                      FROM ( 
                             SELECT MNU_SEQ
                                  , UP_MNU_SEQ
                                  , URL
                                  , MNU_DPTH
                                  , MNU_NM
                                  , SRT_SQN
                                  , US_YN
                                  , UPD_DTTM
                                  , LST_MNU_YN
                               FROM TSYS_ADM_MNU
                              WHERE 1=1
                                AND MNU_DPTH = #{mnu_dpth,jdbcType=NUMERIC}
                                <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn)">AND US_YN           = #{us_yn      ,jdbcType=VARCHAR}</if>
                                <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm)">AND MNU_NM LIKE CONCAT('%',#{mnu_nm    ,jdbcType=VARCHAR},'%')</if>
                                <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(up_mnu_seq)">AND UP_MNU_SEQ = #{up_mnu_seq ,jdbcType=NUMERIC} </if>
                            ) AS T1
            ) AS T2 
             WHERE T2.ROW_NUM BETWEEN  (#{page ,jdbcType=NUMERIC} -1) * #{rows, jdbcType=NUMERIC} +1  AND  #{page ,jdbcType=NUMERIC} * #{rows, jdbcType=NUMERIC}
    </select>

    <!-- 관리자 메뉴 그리드 리스트 조회 -->
    <select id="selectTsysAdmMnuGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuGridList][관리자 메뉴 그리드 리스트 조회 ][TANINE]*/
     SELECT T2.*
          , CEILING(convert(float,T2.TOT_CNT) / convert(float,#{rows, jdbcType=NUMERIC})) AS total_page
          , T2.TOT_CNT AS total_count
          , CEILING(convert(float,T2.ROW_NUM) / convert(float,#{rows, jdbcType=NUMERIC})) AS page
       FROM (
             SELECT T1.*
                  , COUNT(1) OVER() AS TOT_CNT
                  , ROW_NUMBER() OVER(ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN ) ROW_NUM
               FROM ( 
                     SELECT DISTINCT
                            TOP1.MNU_SEQ   AS TOP_MNU_SEQ
                          , TOP1.MNU_NM    AS TOP_MNU_NM
                          , TOP1.SRT_SQN   AS TOP_SRT_SQN
                          , MIDD.MNU_SEQ  AS MIDD_MNU_SEQ
                          , MIDD.MNU_NM   AS MIDD_MNU_NM
                          , MIDD.SRT_SQN  AS MIDD_SRT_SQN
                          , BTM.MNU_SEQ
                          , BTM.MNU_NM
                          , BTM.SRT_SQN  AS BTM_SRT_SQN
                          , BTM.MNU_TP_CD
                          , CASE WHEN MIDD.URL IS NULL THEN BTM.URL END AS URL
                          , CASE WHEN TOP1.US_YN = 'Y'
                                 THEN CASE WHEN MIDD.US_YN = 'Y'
                                           THEN BTM.US_YN ELSE MIDD.US_YN
                                      END
                                 ELSE TOP1.US_YN
                             END AS US_YN
                       FROM TSYS_ADM_MNU TOP1   -- 최상단 메뉴
                          , TSYS_ADM_MNU MIDD  -- 중단 메뉴
                          , TSYS_ADM_MNU BTM   -- 하단 메뉴
                      WHERE TOP1.MNU_SEQ   = MIDD.UP_MNU_SEQ
                        AND MIDD.MNU_SEQ  = BTM.UP_MNU_SEQ
                        AND TOP1.FRNT_YN  = 'N'
                        AND MIDD.FRNT_YN = 'N'
                        AND BTM.FRNT_YN  = 'N'
                     ) AS T1
              WHERE 1=1
              <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm)">
                AND T1.MNU_NM LIKE CONCAT('%',#{mnu_nm    ,jdbcType=VARCHAR},'%')
              </if>
              <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn)">
                AND T1.US_YN = #{us_yn    ,jdbcType=VARCHAR}
              </if>
             ) AS T2 
     WHERE T2.ROW_NUM BETWEEN  (#{page ,jdbcType=NUMERIC} -1) * #{rows, jdbcType=NUMERIC} +1  AND  #{page ,jdbcType=NUMERIC} * #{rows, jdbcType=NUMERIC}
    </select>

    <select id="selectTsysAdmMnuGridTreeList" parameterType="ParameterMap" resultType="TsysAdmMnuTree">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuGridTreeList][관리자 메뉴 그리드 리스트 조회 ][TANINE]*/
          WITH TREE_QUERY AS 
            ( 
             SELECT 0            AS MNU_SEQ
                  , NULL         AS UP_MNU_SEQ
                  , '백오피스'      AS MNU_NM
                  , 0            AS MNU_DPTH
                  , 'Y'          AS US_YN
                  , 'N'          AS MNU_YN
                  , 'N'          AS LST_MNU_YN
                  , 0            AS SRT_SQN
                  , '10'         AS MNU_TP_CD
                  , NULL         AS PGM_ID
                  , CONVERT(VARCHAR(255), 0) SORT 
                  , CONVERT(VARCHAR(255), '백오피스') DEPTH_FULLNAME  
              UNION ALL         
             SELECT SA.MNU_SEQ 
                  , SA.UP_MNU_SEQ 
                  , SA.MNU_NM
                  , SA.MNU_DPTH
                  , SA.US_YN
                  , SA.MNU_YN
                  , SA.LST_MNU_YN
                  , SA.SRT_SQN
                  , SA.MNU_TP_CD
                  , SA.PGM_ID
                  , CONVERT(VARCHAR(255), SA.SRT_SQN) SORT  
                  , CONVERT(VARCHAR(255), SA.MNU_NM) DEPTH_FULLNAME  
               FROM TSYS_ADM_MNU SA
              WHERE 1=1
                AND MNU_DPTH =1
                AND FRNT_YN ='N'
              UNION ALL 
            SELECT B.*
                  , CONVERT(VARCHAR(255), CONVERT(NVARCHAR,C.SORT) +'>'+ CONVERT(VARCHAR(255),DENSE_RANK() OVER(PARTITION BY B.UP_MNU_SEQ  ORDER BY b.SRT_SQN) )) SORT  
                  , CONVERT(VARCHAR(255), CONVERT(NVARCHAR,C.DEPTH_FULLNAME) + ' > ' + CONVERT(VARCHAR(255), B.MNU_NM)) DEPTH_FULLNAME 
              FROM(
              	 SELECT B.MNU_SEQ    
                  , CASE WHEN B.UP_MNU_SEQ =0 THEN NULL 
                        ELSE B.UP_MNU_SEQ 
						  END AS UP_MNU_SEQ
                  , B.MNU_NM     
                  , B.MNU_DPTH   
                  , B.US_YN      
                  , B.MNU_YN     
                  , B.LST_MNU_YN 
                  , B.SRT_SQN    
                  , B.MNU_TP_CD  
                  , B.PGM_ID     
               FROM TSYS_ADM_MNU B
              ) B
              INNER JOIN TREE_QUERY C 
                 ON B.UP_MNU_SEQ = C.MNU_SEQ 
            ) 
       SELECT MNU_SEQ      AS ID
            , CASE WHEN UP_MNU_SEQ = 0 THEN NULL ELSE UP_MNU_SEQ END   AS PID
            , MNU_NM       AS NAME
            , MNU_DPTH
            , A.US_YN
            , MNU_YN
            , CASE WHEN MNU_DPTH = 3 THEN 'false' ELSE  'true' END AS [OPEN]
            , CASE WHEN MNU_DPTH = 3 THEN 'false' ELSE  'true' END AS ISPARENT
            , 'Y'          AS ISSAVED
            , SRT_SQN
            , MNU_TP_CD
            , DEPTH_FULLNAME
            , A.PGM_ID
            , SI.PGM_NM
         FROM TREE_QUERY A 
         LEFT OUTER JOIN  TSYS_PGM_INF  SI
           ON A.PGM_ID = SI.PGM_ID
        ORDER BY SORT
    </select>

    <select id="selectTsysAdmMnuListByUserId" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListByUserId][bo 사용자별 조직 권한 메뉴 리스트 조회  ][TANINE]*/
        SELECT T1.* 
             , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
             , STUFF(( SELECT ','+PB.PGM_BTN_CD 
                         FROM TSYS_PGM_BTN_INF PB 
                        WHERE T1.PGM_ID = PB.PGM_ID
                          FOR XML PATH('')), 1, 1, '')      AS BTN_INFO
            , T1.TOP_MNU_NM + '>' + T1.MIDD_MNU_NM + '>' + T1.MNU_NM AS DEPTH_FULLNAME
          FROM (
                SELECT DISTINCT
                       TOP1.MNU_SEQ    AS TOP_MNU_SEQ
                     , TOP1.MNU_NM     AS TOP_MNU_NM
                     , TOP1.SRT_SQN    AS TOP_SRT_SQN
                     , MIDD.MNU_SEQ   AS MIDD_MNU_SEQ
                     , MIDD.MNU_NM    AS MIDD_MNU_NM
                     , MIDD.SRT_SQN   AS MIDD_SRT_SQN
                     , BTM.MNU_SEQ
                     , BTM.MNU_NM
                     , BTM.SRT_SQN    AS BTM_SRT_SQN
                     , BTM.MNU_TP_CD
                     , CASE WHEN (MIDD.URL IS NULL or TRIM(MIDD.URL) = '') THEN BTM.URL
                        END AS URL
                     , CASE WHEN TOP1.US_YN = 'Y'
                            THEN CASE WHEN MIDD.US_YN = 'Y'
                                      THEN BTM.US_YN ELSE MIDD.US_YN
                                  END
                            ELSE TOP1.US_YN
                        END AS US_YN
                     , CASE WHEN MIDD.PGM_ID IS NULL THEN BTM.PGM_ID END AS PGM_ID
                  FROM TSYS_ADM_MNU TOP1     -- 최상단 메뉴
                     , TSYS_ADM_MNU MIDD    -- 중단 메뉴
                     , TSYS_ADM_MNU BTM     -- 하단 메뉴
                     , TSYS_ADM_ROL_RTNN URA
                     , TSYS_ADM_MNU_ROL_RTNN MRA
                     , TSYS_USR_ROL UR
                 <if test='mngr_tp_cd != "10"'>
                     , TSYS_ORG_ROL_RTNN ORG
                 </if>
                 WHERE TOP1.MNU_SEQ = MIDD.UP_MNU_SEQ
                   AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                   AND TOP1.FRNT_YN = 'N'
                   AND MIDD.FRNT_YN = 'N'
                   AND BTM.FRNT_YN = 'N'
                   AND TOP1.US_YN   = 'Y'
                   AND TOP1.US_YN   = MIDD.US_YN
                   AND TOP1.US_YN   = BTM.US_YN
                   AND URA.ROL_ID  = MRA.ROL_ID
                   AND URA.ROL_ID  = UR.ROL_ID
                   AND MRA.MNU_SEQ = BTM.MNU_SEQ
                   AND CURRENT_TIMESTAMP BETWEEN UR.APL_ST_DT AND UR.APL_ED_DT
                   AND CURRENT_TIMESTAMP BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                   AND UR.US_YN = 'Y'
                   AND URA.USR_ID =  #{user_id ,jdbcType=VARCHAR}
                <if test='mngr_tp_cd != "10"'>
                   AND ORG.ROL_ID = URA.ROL_ID
                   AND ORG.ORG_SEQ =  #{org_seq ,jdbcType=VARCHAR}
                </if>
             ) AS T1
        <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url)">AND V.URL = #{url ,jdbcType=VARCHAR}</if>  
        ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN      
    </select>
    
      <select id="selectTsysAdmMnuListByUsrAuth" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListByUserId][BO사용자별 권한 메뉴 리스트 조회 ][TANINE]*/
        SELECT T1.* 
             , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
             , STUFF(( SELECT ','+PB.PGM_BTN_CD 
                         FROM TSYS_PGM_BTN_INF PB 
                        WHERE T1.PGM_ID = PB.PGM_ID
                          FOR XML PATH('')), 1, 1, '')      AS BTN_INFO
            , T1.TOP_MNU_NM + '>' + T1.MIDD_MNU_NM + '>' + T1.MNU_NM AS DEPTH_FULLNAME
          FROM (
                SELECT DISTINCT
                       TOP1.MNU_SEQ    AS TOP_MNU_SEQ
                     , TOP1.MNU_NM     AS TOP_MNU_NM
                     , TOP1.SRT_SQN    AS TOP_SRT_SQN
                     , MIDD.MNU_SEQ   AS MIDD_MNU_SEQ
                     , MIDD.MNU_NM    AS MIDD_MNU_NM
                     , MIDD.SRT_SQN   AS MIDD_SRT_SQN
                     , BTM.MNU_SEQ
                     , BTM.MNU_NM
                     , BTM.SRT_SQN    AS BTM_SRT_SQN
                     , BTM.MNU_TP_CD
                     , CASE WHEN (MIDD.URL IS NULL or TRIM(MIDD.URL) = '') THEN BTM.URL
                        END AS URL
                     , CASE WHEN TOP1.US_YN = 'Y'
                            THEN CASE WHEN MIDD.US_YN = 'Y'
                                      THEN BTM.US_YN ELSE MIDD.US_YN
                                  END
                            ELSE TOP1.US_YN
                        END AS US_YN
                     , CASE WHEN MIDD.PGM_ID IS NULL THEN BTM.PGM_ID END AS PGM_ID
                  FROM TSYS_ADM_MNU TOP1     -- 최상단 메뉴
                     , TSYS_ADM_MNU MIDD    -- 중단 메뉴
                     , TSYS_ADM_MNU BTM     -- 하단 메뉴
                     , TSYS_ADM_ROL_RTNN URA
                     , TSYS_ADM_MNU_ROL_RTNN MRA
                     , TSYS_USR_ROL UR
                 <if test='mngr_tp_cd != "10"'>
                     , TSYS_ORG_ROL_RTNN ORG
                 </if>
                 WHERE TOP1.MNU_SEQ = MIDD.UP_MNU_SEQ
                   AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                   AND TOP1.FRNT_YN = 'N'
                   AND MIDD.FRNT_YN = 'N'
                   AND BTM.FRNT_YN = 'N'
                   AND TOP1.US_YN   = 'Y'
                   AND TOP1.US_YN   = MIDD.US_YN
                   AND TOP1.US_YN   = BTM.US_YN
                   AND URA.ROL_ID  = MRA.ROL_ID
                   AND URA.ROL_ID  = UR.ROL_ID
                   AND MRA.MNU_SEQ = BTM.MNU_SEQ
                   AND CURRENT_TIMESTAMP BETWEEN UR.APL_ST_DT AND UR.APL_ED_DT
                   AND CURRENT_TIMESTAMP BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                   AND UR.US_YN = 'Y'
                   AND URA.USR_ID =  #{user_id ,jdbcType=VARCHAR}
                <if test='mngr_tp_cd != "10"'>
                   AND ORG.ROL_ID = URA.ROL_ID
                   AND ORG.ORG_SEQ =  #{org_seq ,jdbcType=VARCHAR}
                </if>
             ) AS T1
        <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url)">AND V.URL = #{url ,jdbcType=VARCHAR}</if>  
        ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN      
    </select>

    <!-- 관리자 메뉴 조회 (2018-02-10) -->
    <select id="selectTsysAdmMnuListAll" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListAll][관리자 메뉴 조회 (2018-02-10)][TANINE]*/
         SELECT T1.* 
              , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
          FROM
               ( SELECT DISTINCT
                        TOP1.MNU_SEQ     AS TOP_MNU_SEQ
                      , TOP1.MNU_NM      AS TOP_MNU_NM
                      , TOP1.SRT_SQN     AS TOP_SRT_SQN
                      , MIDD.MNU_SEQ    AS MIDD_MNU_SEQ
                      , MIDD.MNU_NM     AS MIDD_MNU_NM
                      , MIDD.SRT_SQN    AS MIDD_SRT_SQN
                      , BTM.MNU_SEQ
                      , BTM.MNU_NM
                      , BTM.SRT_SQN     AS BTM_SRT_SQN
                      , BTM.MNU_TP_CD
                      , CASE WHEN MIDD.URL IS NULL THEN BTM.URL END AS URL
                      , CASE WHEN TOP1.US_YN = 'Y'
                             THEN CASE WHEN MIDD.US_YN = 'Y' THEN BTM.US_YN ELSE MIDD.US_YN END
                             ELSE TOP1.US_YN
                        END AS US_YN
                   FROM TSYS_ADM_MNU TOP1        -- 최상단 메뉴
                      , TSYS_ADM_MNU MIDD       -- 중단 메뉴
                      , TSYS_ADM_MNU BTM        -- 하단 메뉴
                      , TSYS_USR_ROL_RTNN URA
                      , TSYS_ADM_MNU_ROL_RTNN MRA
                      , TSYS_USR_ROL UR
                  WHERE TOP1.MNU_SEQ = MIDD.UP_MNU_SEQ
                    AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                    AND URA.ROL_ID = MRA.ROL_ID
                    AND URA.ROL_ID = UR.ROL_ID
                    AND TOP1.FRNT_YN = 'N'
                    AND MIDD.FRNT_YN = 'N'
                    AND BTM.FRNT_YN = 'N'
                    AND UR.US_YN = 'Y'
             ) AS T1
        <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url)">AND V.URL = #{url ,jdbcType=VARCHAR}</if>  
        ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN      
    </select>

    <!-- 사용자 아이디별 메뉴 권한 -->
    <select id="selectTsysAdmMnuAuthByUserId" parameterType="ParameterMap" resultType="DataMap">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuAuthByUserId][사용자 아이디별 메뉴 권한][TANINE]*/
        SELECT CASE WHEN SUM(CHK.STD_COUNT) > 0 AND SUM(CHK.EXISTS_COUNT) = 0 THEN 'F' ELSE 'S' END AUTH_RST
             , CHK.MNU_SEQ
          FROM (
                SELECT COUNT(AM.MNU_SEQ) STD_COUNT
                     , COUNT(U.MNU_SEQ)  EXISTS_COUNT
                     , MAX(AM.MNU_SEQ)   MNU_SEQ
                  FROM TSYS_ADM_MNU AM
                  LEFT OUTER JOIN ( SELECT MRA.MNU_SEQ
                                      FROM TSYS_USR_ROL_RTNN URA
                                         , TSYS_USR_ROL UR
                                         , TSYS_ADM_MNU_ROL_RTNN MRA
                                     WHERE UR.ROL_ID = URA.ROL_ID
                                       AND URA.USR_ID = #{usr_id, jdbcType=VARCHAR}
                                       AND UR.ROL_ID = MRA.ROL_ID
                                       AND GETDATE() BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                                       AND GETDATE() BETWEEN UR.APL_ST_DT AND UR.APL_ED_DT
                                       AND UR.US_YN = 'Y') AS U ON U.MNU_SEQ = AM.MNU_SEQ
                  WHERE AM.URL = #{url, jdbcType=VARCHAR}
                AND  AM.US_YN = 'Y'
        ) CHK
    </select>

    <!-- FRONT 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuFrntList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuFrntList][FRONT 메뉴 리스트 조회][TANINE]*/
        SELECT MNU_SEQ
             , UP_MNU_SEQ
             , MNU_NM
             , MNU_DPTH
             , URL
             , MNU_TP_CD
             , MNU_YN
             , US_YN
             , LST_MNU_YN
             , SRT_SQN
             , MNUL_ID
             , MNU_DESC
             , CRTR_ID
             , CRT_DTTM
             , UPDR_ID
             , UPD_DTTM
          FROM TSYS_ADM_MNU
         WHERE FRNT_YN = 'Y'
         ORDER BY MNU_DESC
    </select>

    <select id="selectTsysAdmMnuDownGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuDownGridList][][TANINE]*/
         SELECT T2.*
              , CEILING(CONVERT(FLOAT,T2.TOT_CNT) / CONVERT(FLOAT,#{rows, jdbcType=NUMERIC})) AS TOTAL_PAGE
              , T2.TOT_CNT AS TOTAL_COUNT
              , CEILING(CONVERT(FLOAT,T2.ROW_NUM) / CONVERT(FLOAT,#{rows, jdbcType=NUMERIC})) AS PAGE
           FROM (
                 SELECT T1.*
                      , COUNT(1) OVER() AS TOT_CNT
                      <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
                      , ROW_NUMBER() OVER(ORDER BY T1.${sidx}  ${sord}) ROW_NUM
                      </if>
                      <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">   
                       , ROW_NUMBER() OVER(ORDER BY T1.SRT_SQN ) ROW_NUM
                      </if>
                   FROM ( SELECT A.MNU_SEQ
                               , A.UP_MNU_SEQ
                               , A.MNU_NM
                               , A.MNU_DPTH
                               , A.US_YN
                               , A.MNU_YN
                               , A.SRT_SQN
                               , A.MNU_TP_CD
                               , A.PGM_ID
                               , SI.PGM_NM
                               , SI.TRG_URL
                            FROM TSYS_ADM_MNU A
                            LEFT OUTER JOIN  TSYS_PGM_INF  SI
                              ON A.PGM_ID = SI.PGM_ID
                           WHERE 1=1
                             AND A.UP_MNU_SEQ = #{up_mnu_seq ,jdbcType=NUMERIC}
                             AND A.MNU_SEQ IS NOT NULL
                          ) AS T1
                  ) AS T2 
             WHERE T2.ROW_NUM BETWEEN  (#{page ,jdbcType=NUMERIC} -1) * #{rows, jdbcType=NUMERIC} +1  AND  #{page ,jdbcType=NUMERIC} * #{rows, jdbcType=NUMERIC}
    </select>

    <!-- BO 메뉴 개별 조회 -->
    <select id="selectTsysAdmMnuTgt" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuTgt][BO 메뉴 개별 조회][TANINE]*/
        SELECT MNU_SEQ
             , UP_MNU_SEQ
             , MNU_NM
          FROM TSYS_ADM_MNU
         WHERE FRNT_YN = 'N'
           AND US_YN = 'Y'
           AND URL = #{url ,jdbcType=VARCHAR}
    </select>

    <!-- FO 메뉴 URL 조회 -->
    <select id="selectTsysAdmMnuURL" parameterType="String" resultType="TsysAdmMnu">
    /*[TsysAdmMnu.xml][selectURL][FO 메뉴 URL 조회][TANINE]*/
        SELECT URL FROM TSYS_ADM_MNU WHERE URL LIKE #{url,  jdbcType=VARCHAR}
    </select>

    <!-- 메뉴아이디 얻기 -->
    <select id="selectTsysAdmMnuId" parameterType="ParameterMap" resultType="java.lang.String">
        /*[TsysAdmMnu.xml][selectMenuId][메뉴아이디 얻기][TANINE]*/
       SELECT ISNULL(MAX(MNU_SEQ), 0)
        FROM TSYS_ADM_MNU
       WHERE US_YN = 'Y'
          AND URL = #{url ,jdbcType=VARCHAR}
    </select>
    
    <!-- 공통 > breadCrumb(홈 > 시스템관리 > 코드관리 > 코드관리) 메뉴 조회 -->
    <select id="selectTsysAdmMnuBreadCrumb" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuBreadCrumb][breadCrumb 메뉴조회][TANINE]*/
        SELECT TOP1.MNU_SEQ  AS TOP_MNU_SEQ
             , TOP1.MNU_NM   AS TOP_MNU_NM
             , MIDD.MNU_SEQ AS MIDD_MNU_SEQ
             , MIDD.MNU_NM  AS MIDD_MNU_NM
             , BTM.MNU_SEQ  AS MNU_SEQ
             , BTM.MNU_NM   AS MNU_NM
          FROM TSYS_ADM_MNU TOP1
         INNER JOIN TSYS_ADM_MNU MIDD 
            ON (TOP1.MNU_SEQ = MIDD.UP_MNU_SEQ)
         INNER JOIN TSYS_ADM_MNU BTM 
            ON (MIDD.MNU_SEQ = BTM.UP_MNU_SEQ)
         WHERE BTM.MNU_SEQ = #{mnu_seq ,jdbcType=NUMERIC}
    </select>
    
    <!-- 권한의 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuRolRtnnGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuRolRtnnGridList][권한의 메뉴 리스트 조회][TANINE]*/
		SELECT RM.ROL_ID						AS ROL_ID
			 , RM.MNU_SEQ						AS MNU_SEQ
			 , MN.MNU_NM						AS MNU_NM
			 , MN.UP_MNU_SEQ					AS MIDD_MNU_SEQ
			 , (SELECT MNU_NM
				  FROM TSYS_ADM_MNU
				 WHERE MNU_SEQ = MN.UP_MNU_SEQ)	AS MIDD_MNU_NM
			 , MN.SRT_SQN						AS SRT_SQN
			 , MN.PGM_ID						AS PGM_ID
		  FROM TSYS_ADM_MNU				MN
			 , TSYS_ADM_MNU_ROL_RTNN	RM
		 WHERE MN.MNU_SEQ	= RM.MNU_SEQ
		   AND MN.MNU_DPTH	= 3
		   AND MN.FRNT_YN	= 'N'
		   AND MN.MNU_YN	= 'Y'
		   AND MN.US_YN		= 'Y'
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rol_id)">
		   AND RM.ROL_ID	= #{rol_id, jdbcType=VARCHAR}
		</if>
		 ORDER BY MIDD_MNU_SEQ, SRT_SQN
	</select>
	
	<!-- 권한 메뉴의 버튼 리스트 조회 -->
    <select id="selectTsysAdmMnuRolRtnnBtnGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuRolRtnnBtnGridList][권한 메뉴의 버튼 리스트 조회][TANINE]*/
		  SELECT STUFF((SELECT ',' + PB.PGM_BTN_CD
			              FROM TSYS_PGM_BTN_INF PB
			             WHERE PB.PGM_ID = T1.PGM_ID
			             ORDER BY PB.PGM_BTN_CD
			               FOR XML PATH('')), 1, 1, '') AS PGM_BTN
			   , STUFF((SELECT ',' + RB.PGM_BTN_CD
			            FROM TSYS_ORG_BTN_ROL_RTNN RB
			           WHERE RB.PGM_ID = T1.PGM_ID
		                 AND RB.ROL_ID =  #{rol_id, jdbcType=VARCHAR}
		                 AND RB.MNU_SEQ = #{mnu_seq, jdbcType=NUMERIC}
		               ORDER BY RB.PGM_BTN_CD
		                 FOR XML PATH('')), 1, 1, '') AS ROL_BTN
		   FROM TSYS_PGM_INF T1
		  WHERE T1.PGM_ID = #{pgm_id, jdbcType=VARCHAR}
		    AND T1.US_YN = 'Y'
	</select>
    
    <!-- 권한 추가를 위한 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuBtnList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuBtnList][권한 추가 대상 메뉴/버튼 조회][TANINE]*/
        SELECT P.*
		  FROM (SELECT V.*
					 , CEIL(V.ID / #{rows, jdbcType=NUMERIC})			AS PAGE
					 , @ROW_NUMBER										AS TOTAL_COUNT
					 , CEIL(@ROW_NUMBER / #{rows, jdbcType=NUMERIC})	AS TOTAL_PAGE
				  FROM (SELECT MN.*
							 , (@ROW_NUMBER := @ROW_NUMBER + 1) ID 
						  FROM (SELECT MN.MNU_SEQ						AS MNU_SEQ
									 , MN.MNU_NM						AS MNU_NM
									 , MN.UP_MNU_SEQ					AS MIDD_MNU_SEQ
									 , (SELECT MNU_NM
										  FROM TSYS_ADM_MNU
										 WHERE MNU_SEQ = MN.UP_MNU_SEQ)	AS MIDD_MNU_NM
									 , MN.SRT_SQN						AS SRT_SQN
									 , MN.PGM_ID						AS PGM_ID
									 , (SELECT GROUP_CONCAT(PGM_BTN_CD SEPARATOR ',')
										  FROM TSYS_PGM_BTN_INF PB
										 WHERE PB.PGM_ID = MN.PGM_ID
										   AND US_YN = 'Y')				AS PGM_BTN
									 , MN.MNU_DPTH						AS MNU_DPTH
								  FROM TSYS_ADM_MNU	MN
								 WHERE 1 = 1
								   AND MN.FRNT_YN	= 'N'
								   AND MN.MNU_YN	= 'Y'
								   AND MN.US_YN		= 'Y'
								   AND ISNULL(MN.PGM_ID,'') != ''
								   AND ISNULL(MN.MNU_SEQ,'') != ''
						 		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm)">
								   AND MN.MNU_NM LIKE CONCAT('%', #{mnu_nm, jdbcType=VARCHAR}, '%')
								</if>
							   ) MN
							 , (SELECT @ROW_NUMBER := 0) RN
						 WHERE MN.MNU_DPTH = 3
					   ) V
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
				 ORDER BY ${sidx} ${sord}
				</if>
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">
				 ORDER BY MIDD_MNU_SEQ, SRT_SQN
				</if>
		  	   ) P
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(page)">
         WHERE P.PAGE = #{page ,jdbcType=NUMERIC}
         </if>
	</select>
</mapper>