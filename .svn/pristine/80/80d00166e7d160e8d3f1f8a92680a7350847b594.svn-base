<%-- 
    작성자   : HIJ
    개요     : 마이페이지 > 회원정보 변경 > 수정입력폼 
    수정사항 :
        2017-11-28 최초작성
        2017-12-14 화면수정
--%>
<?xml version="1.0" encoding="UTF-8" ?>
<%@ page language="java" contentType="text/html; charset=UTF-8"
    pageEncoding="UTF-8"%>
<!-- meta File include -->
<%@ include file="/WEB-INF/views/pandora3Front/common/include/meta.jsp" %>
<script type="text/javascript">
var certEmailFlag = false;
var chgEmail;
var orgEmail = '${myPageUserInfoMap.email}';
var login_id = '${myPageUserInfoMap.login_id}';
<%-- var certKey = '<%=parameterMap.getValue("certKey")%>'; --%>
$(document).ready(function() {
	// Error Msg 감추기
	$(".error-msg").hide();
	
	// 유효성 검사 Error Msg 초기화
	$('input.mod_form').each(function() {
		$(this).focusout(function() {
			$(this).parent().parent().find('p').hide(); 
		})
	});
	
	// 이메일 유효성 검사 Error Msg 초기화
	$("#email").focusout(function() {
		$("#email").parent().parent().find('p').hide(); 
	});
	
	// 개인정보 설정 
	$('input:radio[name=mailing_yn]:input[value="${myPageUserInfoMap.mailing_yn}"]').attr("checked", true);
	$("#pw_question").val("${myPageUserInfoMap.pw_question}");
	
	// 이메일 인증 버튼
	$("#emailCertBtn").click(function() {
		
		// 이메일 인증메일 전송여부 확인
		var flag = $("#email").attr("disabled");
		
		// 입력 데이터 유효성 검사를 통과하면 이메일 인증처리 시작
		if(flag) {
			alert("이미 인증메일이 발송되었습니다.");
		}
		else if(validEmailChk()) {
			jQuery.ajax({
				url: _context + "/main/myPage/myPageEmailUpdateProcess.do",
				type: "POST",
				data: {email : $("#email").val()},
				dataType:'json',
				async:false,
				success: function(data){
					if (data.RESULT == _boolean_success) {
						$("#email").trigger("focusout");
						alert("등록하신 메일로 인증메일이 발송되었습니다\n메일을 확인하세요");
						$("#email").prop("disabled", true);
					}
					else if (data.RESULT == 'P1') {
						alert("사용중인 이메일주소 입니다.");
					}
					else {
						alert('이메일 변경작업이 정상적으로 처리되지 않았습니다');
					}
				}
			}); 
		}
	});
	
	
	// 회원정보 변경 버튼
	$("#update_btn").click(function() {
		if(validInputForm()) {
			// 이메일 인증 완료 확인
			$.ajax({
				url: _context + '/main/myPage/chkChgCertEmail',
				type : "POST",
				data : { email : $("#email").val(), login_id : login_id},
				dataType : "json",
				success: function(data){
					if(data.isEmailCd == '02') {
						if(confirm("이메일 변경인증이 진행중입니다.\n(인증절차를 완료하지 않으시면 이메일주소는 변경되지 않습니다.)\n회원정보를 변경하시겠습니까?")) {
							formSubmit();
						}
					}
					else if(data.isEmailCd == '03') {
						if(confirm("이메일 변경인증을 완료하지 않으시면 이메일주소는 변경되지 않습니다.\n회원정보를 변경하시겠습니까?")) {
							formSubmit();
						}
					} else {
						formSubmit();	
					}
				}
			}); 	
		}
	});
	
	// 취소버튼
	$("#cancel_btn").click(function() {
		clickCancel();
	});

});

// 이메일 인증 유효성 검사
function validEmailChk() {
	var email = $("#email").val();
	// 메일에 변경사항이 있는지 체크
	if(email == orgEmail) {
		var msg = "이메일주소에 변경사항이 없습니다.";
		$("#email").parent().parent().find('p').text(msg).show();
		$("#email").focus();
		return false;
	}
	// 이메일 필수값 체크
	if(email.length == 0) {
		var msg = "이메일을 입력해주세요";
		$("#email").parent().parent().find('p').text(msg).show();
		$("#email").focus();
		return false;
	}
	// 이메일 양식 체크
	var reg = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
	if (!reg.test(email)){
		var msg = "이메일 양식이 잘못되었습니다."
		$("#email").parent().parent().find('p').text(msg).show();
		$("#email").focus();
		return false;
	}
	
	return true;
}

// Input Form 유효성 검사
function validInputForm() {
	var validFlag = true;
	// INPUT FORM CHECK
	$('input.mod_form').each(function() {
		var id = $(this).attr("id");
		var value = $(this).val();
		// 필수체크
		if(isEmpty(value)) {
			$("#"+id).parent().parent().find('p').show();
			$("#"+id).focus();
			validFlag = false;
		}
	});
	return validFlag;
}

// 회원정보 변경 폼 전송
function formSubmit() {
	// 이메일 정보는 별도 변경임으로 파라미터에서 제외
	$("#email").prop("disabled", true);
	// 회원정보 폼 Submit
	$('#userInfo').attr("action", _context +"/main/myPage/myPageInfoUpdateProcess.do" ).submit();
}

// 취소버튼 클릭
function clickCancel() {
	// 회원정보페이지로 이동
	$('#userInfo').attr("action", _context +"/main/myPage/myPageUserInfo.do" ).submit();
}
</script>
<body>
<!-- wrapper -->
<div id="wrapper">
	<!-- header -->
	<%@ include file="/WEB-INF/views/pandora3Front/common/include/header.jsp" %>
	<!--// header -->
	
	<!-- location -->
	<div class="bread-wrap">
	<jsp:include page="/WEB-INF/views/pandora3Front/common/include/common/myPage/myPageTopNav.jsp" flush="false">
		<jsp:param value="회원정보보기" name="submenuActive"/>
	</jsp:include>
	</div>
	<!--// location -->

	<!-- container -->
	<!-- contents-container-withsub research-part -->
	<div class="contents-container-withsub research-part">
		<!-- snb -->
		<div class="contents-submenu ">
		<jsp:include page="/WEB-INF/views/pandora3Front/common/include/common/template.jsp" flush="false">
			<jsp:param value="/WEB-INF/views/pandora3Front/common/include/common/myPage/myPageLeft.jsp" name="url"/>
		</jsp:include>
		</div>
		<!-- //lnb -->
		<!-- contents-main -->
		<div class="contents-main">
			<!-- category title -->
			<h1 class="page-header">회원정보 변경</h1>
			<!--// category title -->
			<nav class="tab tabs-3"> 
                <ul>
                    <li class="active item">
                        <a href="/pandora3/main/myPage/myPageUserInfo.do">회원정보보기</a>
                    </li>
                    <li class="item">
                        <a href="/pandora3/main/myPage/myPageScrapList.do">스크랩보기</a>
                    </li>
                    <li class="item">
                        <a href="/pandora3/main/myPage/memberLeavePage.do">탈퇴하기</a>
                    </li>
                </ul>
            </nav>
			<!-- form -->
			<form name="userInfo" id="userInfo" submit="return false;">
				<div class="join-form">
					<div class="row">
						<div class="col-md-3">
							이메일주소<span class="required">*</span>
						</div>
						<div class="col-md-4 table-border input_infoEdit">
							<input class="form-control" id="email" name="email"  type="text" value="${myPageUserInfoMap.email}" onkeyup="f_chk_byte(this,40)" placeholder="이메일을 입력하세요.">
							<p class="error-msg m-l-0"></p>
						</div>
						<div class="col-md-5 btn_infoEdit">
							<button type="button" class="btn btn-danger btn-xsmall" id="emailCertBtn">이메일 변경인증</button>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-3">
							아이디<span class="required">*</span>
						</div>
						<div class="col-md-4 table-border">
							<p class="form-control-static">${myPageUserInfoMap.login_id}</p>
						</div>
						<div class="col-md-5">
							<p class="error-msg">사용할 수 없는 아이디 입니다.</p>
						</div>
					</div>
	
					<div class="row">
						<div class="col-md-3">
							이름<span class="required">*</span>
						</div>
						<div class="col-md-4 table-border">
							<input id="user_nm" name="user_nm" class="form-control mod_form" type="text" value="${myPageUserInfoMap.user_nm}" onkeyup="f_chk_byte(this,40)" placeholder="이름을 입력하세요.">
						</div>
						<div class="col-md-5">
							<p class="error-msg">이름을 입력하세요.</p>
						</div>
					</div>
					
					<div class="row">
						<div class="col-md-3">
							비밀번호찾기 질문/답변 <span class="required">*</span>
						</div>
						<div class="col-md-4 table-border">
							<%=CodeUtil.getSelectComboList("PQST", "pw_question", "", "", "", "class=\"form-control signForm\"")%>
							<input id="pw_answer" name="pw_answer"  class="form-control m-t-5 mod_form" type="text" value="${myPageUserInfoMap.pw_answer}" onkeyup="f_chk_byte(this,250)" placeholder="비밀번호 찾기 답변을 입력하세요.">
						</div>
						<div class="col-md-4">
							<p class="error-msg">비밀번호 찾기 답변을 입력하세요.</p>
						</div>
					</div>
					<div class="row">
						<div class="col-md-3">
							이메일 수신 동의
						</div>
						<div class="col-md-4 table-border">
							<input type="radio" id="mailing_y" name="mailing_yn" value="Y" ><label for="mailing_y">&nbsp;예</label>&nbsp;&nbsp;&nbsp;&nbsp;
							<input type="radio" id="mailing_n" name="mailing_yn" value="N" ><label for="mailing_n">&nbsp;아니요</label>
						</div>
						<div class="col-md-5">
							<p class="error-msg"> 이메일 수신 동의를 선택하세요.</p>
						</div>
					</div>
				</div>
				<input type="hidden" id="certKey" name="certKey" value="${certKey}" />
			</form>
			<!--// form -->
			
			<!-- button -->
			<div class="buttons text-center m-t-50 m-b-80">
				<button class="btn-primary btn-big m-r-10" id="update_btn" type="button">회원정보 변경</button>
				<button class="btn-default btn-big" id="cancel_btn" type="button">취소</button>
			</div>
			<!--// button -->
		</div>
		<!-- //contents-main -->
	</div>
	<!-- // contents-container-withsub research-part -->
	<!-- footer -->
	<footer id="footer">
		<%@ include file="/WEB-INF/views/pandora3Front/common/include/footer.jsp" %>
	</footer>
	<!-- // footer -->
</div>
<!-- //wrapper -->
</body>
</html>