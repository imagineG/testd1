/**
* <pre>
* 1. 프로젝트명 : 판도라 패키징
* 2. 패키지명   : kr.co.ta9.pandora3.content.manager
* 3. 파일명     : SysCommentsMgr
* 4. 작성일     : 2018-01-10
* </pre>
*/
package kr.co.ta9.pandora3.front.content.manager;

import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ta9.pandora3.app.entry.UserInfo;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.common.util.TextUtil;
import kr.co.ta9.pandora3.pbbs.dao.TbbsCmtInfDao;
import kr.co.ta9.pandora3.pbbs.dao.TbbsCmtInfDaoTrx;
import kr.co.ta9.pandora3.pcommon.dto.TbbsCmtInf;

/**
* <pre>
* 1. 패키지명 : kr.co.ta9.pandora3.content.manager
* 2. 타입명   : class
* 3. 작성일   : 2018-01-10
* 4. 설명     : 댓글 매니저
* </pre>
*/
@Service
public class SysCommentsMgr {
	
	@Autowired
	private TbbsCmtInfDao tbbsCmtInfDao;
	@Autowired
	private TbbsCmtInfDaoTrx tbbsCmtInfDaoTrx;
	
	public List<TbbsCmtInf> selectTbbsCmtInfList(ParameterMap parameterMap) throws Exception {
		return tbbsCmtInfDao.selectTbbsCmtInfList(parameterMap);
	}
	
	public Map<String, Integer> getTbbsCmtInfPagingInfo(List<TbbsCmtInf> docList, ParameterMap param) throws Exception {
		Integer tot_page = 1;
		Integer tot_count = 0;
		int index = 0;
		if(docList.size() > 0) {
			tot_page = docList.get(index).getTotal_page();
			tot_count = Integer.parseInt(String.valueOf(docList.get(index).getTotal_count()));
		} 
		int page = Integer.parseInt(param.get("page").toString());
		
		// DeskTop
		int group_no = page / 10 + (page % 10 > 0 ? 1 : 0);
		int page_eno = group_no * 10;	
		int page_sno = page_eno - (10 - 1);
		if(page_eno>tot_page) page_eno = tot_page;

		// Mobile
		int group_no_m = page / 5 + (page % 5 > 0 ? 1 : 0);
		int page_eno_m = group_no_m * 5;	
		int page_sno_m = page_eno_m - (5 - 1);
		if(page_eno_m>tot_page) page_eno_m = tot_page;
		
		Map<String, Integer> pagingInfo = new HashMap<String,Integer>();
		pagingInfo.put("tot_page", tot_page);
		pagingInfo.put("tot_count", tot_count);
		pagingInfo.put("page", page);
		pagingInfo.put("page_sno", page_sno);
		pagingInfo.put("page_eno", page_eno);
		pagingInfo.put("page_sno_m", page_sno_m);
		pagingInfo.put("page_eno_m", page_eno_m);
		pagingInfo.put("rows", Integer.parseInt(param.get("rows").toString()));
		
		return pagingInfo;
	}
	
	public int insertTbbsCmtInf(ParameterMap parameterMap) throws Exception {
		TbbsCmtInf tbbsCmtInf = (TbbsCmtInf)parameterMap.populate(TbbsCmtInf.class);
		tbbsCmtInf.setCts(TextUtil.removeXss(tbbsCmtInf.getCts()));
		UserInfo userInfo = parameterMap.getUserInfo();
		String user_id = userInfo.getUser_id();
		String ipaddress = userInfo.getLast_access_ip_addr();
		tbbsCmtInf.setCrtr_id(user_id);
		tbbsCmtInf.setIp_addr(ipaddress);
		int result = tbbsCmtInfDaoTrx.insert(tbbsCmtInf);
		int cur_page = 1;
		if(result > 0) {
			parameterMap.put("comment_srl", tbbsCmtInf.getCmt_seq());
			cur_page = tbbsCmtInfDao.selectTbbsCmtInfCurPageInfo(parameterMap);
		}
		return cur_page;
	}
	
	public int updateTbbsCmtInfInfo(ParameterMap parameterMap) throws Exception {
		TbbsCmtInf tbbsCmtInf = (TbbsCmtInf)parameterMap.populate(TbbsCmtInf.class);
		tbbsCmtInf.setCts(TextUtil.removeXss(tbbsCmtInf.getCts()));
		UserInfo userInfo = parameterMap.getUserInfo();
		String user_id = userInfo.getUser_id();
		tbbsCmtInf.setCrtr_id(user_id);
		int result = tbbsCmtInfDaoTrx.updateSysCommentsInfo(tbbsCmtInf);
		int cur_page = 1;
		if(result > 0) {
			parameterMap.put("comment_srl", tbbsCmtInf.getCmt_seq());
			cur_page = tbbsCmtInfDao.selectTbbsCmtInfCurPageInfo(parameterMap);
		}
		return cur_page;
	}
	
	public int deleteTbbsCmtInfInfo(ParameterMap parameterMap) throws Exception {
		TbbsCmtInf sysComments = (TbbsCmtInf)parameterMap.populate(TbbsCmtInf.class);
		int result = tbbsCmtInfDaoTrx.deleteSysCommentsInfo(sysComments);
		if("Y".equals(parameterMap.getValue("upDelYn"))) {
			result += tbbsCmtInfDaoTrx.deleteSysCommentsInfo(sysComments);
		}
		
		int cur_page = 1;
		if(result > 0) {
			cur_page = tbbsCmtInfDao.selectTbbsCmtInfCurPageInfo2(parameterMap);
		}
		return cur_page;
	}
	
}
