<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="TsysAdmMnu">
    <!-- 관리자 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuList" parameterType="ParameterMap" resultType="TsysAdmMnu">
       /*[TsysAdmMnu.xml][selectTsysAdmMnuList][관리자 메뉴 리스트 조회][TANINE]*/
        SELECT MNU_SEQ             -- 메뉴번호
             , UP_MNU_SEQ          -- 상위메뉴아이디
             , MNU_NM              -- 메뉴명
             , MNU_DPTH            -- 메뉴깊이
             , URL                 -- url
             , MNU_TP_CD           -- 메뉴타입코드
             , MNU_YN              -- 메뉴여부
             , US_YN               -- 사용여부
             , LST_MNU_YN          -- 마지막메뉴여부
             , SRT_SQN             -- 정렬순서(전시순서)
             , MNUL_ID             -- 메뉴얼ID
             , MNU_DESC            -- 메뉴설명
             , CRTR_ID             -- 생성자ID
             , CRT_DTTM            -- 생성일시
             , UPDR_ID             -- 수정자ID
             , UPD_DTTM            -- 수정일시
          FROM TSYS_ADM_MNU
         WHERE 1=1
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(up_mnu_seq)">AND UP_MNU_SEQ = #{up_mnu_seq  ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_tp_cd )">AND MNU_TP_CD  = #{mnu_tp_cd   ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_dpth  )">AND MNU_DPTH   = #{mnu_dpth    ,jdbcType=NUMERIC}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn     )">AND US_YN      = #{us_yn       ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_yn    )">AND MNU_YN     = #{mnu_yn      ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm    )">AND MNU_NM LIKE '%' || #{mnu_nm,jdbcType=VARCHAR} || '%'</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url       )">AND URL        = #{url         ,jdbcType=VARCHAR}</if>
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(frnt_yn   )">AND FRNT_YN    = #{frnt_yn     ,jdbcType=VARCHAR}</if>
     ORDER BY UP_MNU_SEQ, MNU_NM
    </select>

    <!-- 관리자 시스템 메뉴 그리드 리스트 조회 -->
    <select id="selectTsysAdmMnuDepthGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuDepthGridList][관리자 시스템 메뉴 그리드 리스트 조회 ][TANINE]*/
     <include refid="Common.getPaging-Prefix" />
         SELECT MNU_SEQ
              , UP_MNU_SEQ
              , URL
              , MNU_DPTH
              , MNU_NM
              , SRT_SQN
              , US_YN
              , UPD_DTTM
              , LST_MNU_YN
           FROM TSYS_ADM_MNU
          WHERE 1=1
            AND MNU_DPTH = #{mnu_dpth,jdbcType=NUMERIC}
            <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn     )">AND US_YN      = #{us_yn      ,jdbcType=VARCHAR}</if>
            <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm    )">AND MNU_NM LIKE '%' || #{mnu_nm   ,jdbcType=VARCHAR} || '%'</if>
            <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(up_mnu_seq)">AND UP_MNU_SEQ = #{up_mnu_seq ,jdbcType=NUMERIC} </if>
          <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
          ORDER BY  ${sidx}  ${sord}
          </if>
          <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">   
          ORDER BY MNU_SEQ 
          </if>
      <include refid="Common.getPaging-Suffix" />
    </select>

    <!-- 관리자 메뉴 그리드 리스트 조회 -->
    <select id="selectTsysAdmMnuGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuGridList][관리자 메뉴 그리드 리스트 조회 ][TANINE]*/
     <include refid="Common.getPaging-Prefix" />
         SELECT A.*
           FROM (
                  SELECT DISTINCT
                         TOP.MNU_SEQ   AS TOP_MNU_SEQ
                       , TOP.MNU_NM    AS TOP_MNU_NM
                       , TOP.SRT_SQN   AS TOP_SRT_SQN
                       , MIDD.MNU_SEQ  AS MIDD_MNU_SEQ
                       , MIDD.MNU_NM   AS MIDD_MNU_NM
                       , MIDD.SRT_SQN  AS MIDD_SRT_SQN
                       , BTM.MNU_SEQ
                       , BTM.MNU_NM
                       , BTM.SRT_SQN   AS BTM_SRT_SQN
                       , BTM.MNU_TP_CD
                       , CASE WHEN MIDD.URL IS NULL THEN BTM.URL END AS URL
                       , CASE WHEN TOP.US_YN = 'Y'
                              THEN CASE WHEN MIDD.US_YN = 'Y'
                                        THEN BTM.US_YN ELSE MIDD.US_YN
                                    END
                              ELSE TOP.US_YN
                          END AS US_YN
                       , TOP.SIT_SEQ 	AS SIT_SEQ   
                    FROM TSYS_ADM_MNU TOP   -- 최상단 메뉴
                       , TSYS_ADM_MNU MIDD  -- 중단 메뉴
                       , TSYS_ADM_MNU BTM   -- 하단 메뉴
                   WHERE TOP.MNU_SEQ   = MIDD.UP_MNU_SEQ
                     AND MIDD.MNU_SEQ  = BTM.UP_MNU_SEQ
                     AND TOP.FRNT_YN  = 'N'
                     AND MIDD.FRNT_YN = 'N'
                     AND BTM.FRNT_YN  = 'N'
                 ) A
          WHERE 1=1
           <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(menu_nm)">
            AND A.MENU_NM LIKE  '%' || #{menu_nm    ,jdbcType=VARCHAR} ||  '%' 
           </if>
           <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(us_yn)">
            AND A.US_YN = #{us_yn    ,jdbcType=VARCHAR}
           </if>
           <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_seq)">
            AND A.SIT_SEQ = #{sit_seq    ,jdbcType=NUMERIC}
           </if>
      ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN
     <include refid="Common.getPaging-Suffix" />
    </select>

    <select id="selectTsysAdmMnuGridTreeList" parameterType="ParameterMap" resultType="TsysAdmMnuTree">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuGridTreeList][관리자 메뉴 그리드 리스트 조회 ][TANINE]*/
        SELECT   T1.MNU_SEQ     AS ID 
               , T1.UP_MNU_SEQ  AS PID
               , T1.MNU_NM      AS NAME
               , T1.MNU_DPTH
               , T1.US_YN
               , T1.MNU_YN
               , T1.SRT_SQN
               , T1.MNU_TP_CD
               , T1.PGM_ID
               , SI.PGM_NM
              , CASE WHEN T1.MNU_DPTH = 3 THEN 0
                     ELSE  1
                 END AS OPEN
              , CASE WHEN T1.MNU_DPTH = 3 THEN 0
                      ELSE  1
                END AS ISPARENT
              , 'Y' AS ISSAVED
              , SUBSTR(SYS_CONNECT_BY_PATH(T1.MNU_NM, '>'),2) AS DEPTH_FULLNAME
        FROM
        (
            SELECT  0  AS MNU_SEQ
                   , NULL         AS UP_MNU_SEQ
                   , '백오피스'      AS MNU_NM
                   , 0            AS MNU_DPTH
                   , 'Y'          AS US_YN
                   , 'N'          AS MNU_YN
                   , 'N'          AS LST_MNU_YN
                   , 0            AS SRT_SQN
                   , '10'         AS MNU_TP_CD
                   , NULL         AS PGM_ID
            FROM DUAL
            UNION ALL         
            SELECT    B.MNU_SEQ 
                    , CASE WHEN B.UP_MNU_SEQ=0 THEN NULL ELSE B.UP_MNU_SEQ END AS UP_MNU_SEQ
                    , B.MNU_NM
                    , B.MNU_DPTH 
                    , B.US_YN
                    , B.MNU_YN
                    , B.LST_MNU_YN
                    , B.SRT_SQN
                    , B.MNU_TP_CD
                    , B.PGM_ID
            FROM  TSYS_ADM_MNU B
            WHERE FRNT_YN ='N'
              AND B.SIT_SEQ = #{sit_seq, jdbcType=VARCHAR}
        )  T1
         , TSYS_PGM_INF SI
         WHERE T1.PGM_ID = SI.PGM_ID(+)
        CONNECT BY PRIOR T1.MNU_SEQ = T1.UP_MNU_SEQ
        START WITH NVL(UP_MNU_SEQ,'0') = 0
        ORDER SIBLINGS BY SRT_SQN	
    </select>

    <select id="selectTsysAdmMnuListByUserId" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListByUserId][조직별 관리자 메뉴 조회][TANINE]*/
           SELECT  T1.* 
                 , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
                 , (SELECT TO_CHAR(WM_CONCAT(PB.PGM_BTN_CD))  FROM TSYS_PGM_BTN_INF PB WHERE PB.PGM_ID = T1.PGM_ID GROUP BY  PGM_ID )   AS   BTN_INFO
                 , T1.TOP_MNU_NM || '>' || T1.MIDD_MNU_NM || '>' || T1.MNU_NM AS DEPTH_FULLNAME  
            FROM
            (
                SELECT DISTINCT
                       TOP.MNU_SEQ    AS TOP_MNU_SEQ
                     , TOP.MNU_NM     AS TOP_MNU_NM
                     , TOP.SRT_SQN    AS TOP_SRT_SQN
                     , MIDD.MNU_SEQ   AS MIDD_MNU_SEQ
                     , MIDD.MNU_NM    AS MIDD_MNU_NM
                     , MIDD.SRT_SQN   AS MIDD_SRT_SQN
                     , BTM.MNU_SEQ
                     , BTM.MNU_NM
                     , BTM.SRT_SQN    AS BTM_SRT_SQN
                     , BTM.MNU_TP_CD
                     , CASE WHEN (MIDD.URL IS NULL or TRIM(MIDD.URL) = '') THEN BTM.URL
                        END AS URL
                     , CASE WHEN TOP.US_YN = 'Y'
                            THEN CASE WHEN MIDD.US_YN = 'Y'
                                      THEN BTM.US_YN ELSE MIDD.US_YN
                                  END
                            ELSE TOP.US_YN
                        END AS US_YN
                     , CASE WHEN MIDD.PGM_ID IS NULL THEN BTM.PGM_ID END AS PGM_ID
                  FROM TSYS_ADM_MNU TOP     -- 최상단 메뉴
                     , TSYS_ADM_MNU MIDD    -- 중단 메뉴
                     , TSYS_ADM_MNU BTM	    -- 하단 메뉴
                     , TSYS_ADM_ROL_RTNN URA
                     , TSYS_ADM_MNU_ROL_RTNN MRA
                     , TSYS_ADM_ROL AR
                 <if test='mngr_tp_cd != "10"'>
                     , TSYS_ORG_ROL_RTNN ORG
                 </if>
                 WHERE TOP.MNU_SEQ = MIDD.UP_MNU_SEQ
                   AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                   AND TOP.FRNT_YN = 'N'
                   AND MIDD.FRNT_YN = 'N'
                   AND BTM.FRNT_YN = 'N'
                   AND TOP.US_YN   = 'Y'
                   AND TOP.US_YN   = MIDD.US_YN
                   AND TOP.US_YN   = BTM.US_YN
                   AND URA.ROL_ID  = MRA.ROL_ID
                   AND URA.ROL_ID  = AR.ROL_ID
                   AND MRA.MNU_SEQ = BTM.MNU_SEQ
                   AND TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMMDD') BETWEEN AR.APL_ST_DT AND AR.APL_ED_DT
                   AND TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMMDD') BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                   AND AR.US_YN = 'Y'
                   AND URA.USR_ID =  #{user_id ,jdbcType=VARCHAR}
                <if test='mngr_tp_cd != "10"'>
                   AND ORG.ROL_ID = URA.ROL_ID
                   AND ORG.ORG_SEQ =  #{org_seq ,jdbcType=VARCHAR}
                </if>
              ) T1
           ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN
	</select>
	
	    <select id="selectTsysAdmMnuListByUsrAuth" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListByUsrAuth][사용자 ID별 관리자 메뉴 조회][TANINE]*/
           SELECT  T1.* 
                 , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
                 , (SELECT TO_CHAR(WM_CONCAT(PB.PGM_BTN_CD))  FROM TSYS_PGM_BTN_INF PB WHERE PB.PGM_ID = T1.PGM_ID GROUP BY  PGM_ID )   AS   BTN_INFO
                 , T1.TOP_MNU_NM || '>' || T1.MIDD_MNU_NM || '>' || T1.MNU_NM AS DEPTH_FULLNAME  
            FROM
            (
                SELECT DISTINCT
                       TOP.MNU_SEQ    AS TOP_MNU_SEQ
                     , TOP.MNU_NM     AS TOP_MNU_NM
                     , TOP.SRT_SQN    AS TOP_SRT_SQN
                     , MIDD.MNU_SEQ   AS MIDD_MNU_SEQ
                     , MIDD.MNU_NM    AS MIDD_MNU_NM
                     , MIDD.SRT_SQN   AS MIDD_SRT_SQN
                     , BTM.MNU_SEQ
                     , BTM.MNU_NM
                     , BTM.SRT_SQN    AS BTM_SRT_SQN
                     , BTM.MNU_TP_CD
                     , CASE WHEN (MIDD.URL IS NULL or TRIM(MIDD.URL) = '') THEN BTM.URL
                        END AS URL
                     , CASE WHEN TOP.US_YN = 'Y'
                            THEN CASE WHEN MIDD.US_YN = 'Y'
                                      THEN BTM.US_YN ELSE MIDD.US_YN
                                  END
                            ELSE TOP.US_YN
                        END AS US_YN
                     , CASE WHEN MIDD.PGM_ID IS NULL THEN BTM.PGM_ID END AS PGM_ID
                  FROM TSYS_ADM_MNU TOP     -- 최상단 메뉴
                     , TSYS_ADM_MNU MIDD    -- 중단 메뉴
                     , TSYS_ADM_MNU BTM	    -- 하단 메뉴
                     , TSYS_ADM_ROL_RTNN URA
                     , TSYS_ADM_MNU_ROL_RTNN MRA
                     , TSYS_ADM_ROL AR
                     , TSYS_ADM_MNU_RTNN MRN  --사용자별 메뉴권한 매핑
                 WHERE TOP.MNU_SEQ = MIDD.UP_MNU_SEQ
                   AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                   AND TOP.FRNT_YN = 'N'
                   AND MIDD.FRNT_YN = 'N'
                   AND BTM.FRNT_YN = 'N'
                   AND TOP.US_YN   = 'Y'
                   AND TOP.US_YN   = MIDD.US_YN
                   AND TOP.US_YN   = BTM.US_YN
                   AND URA.ROL_ID  = MRA.ROL_ID
                   AND URA.ROL_ID  = AR.ROL_ID
                   AND MRA.MNU_SEQ = BTM.MNU_SEQ
                   AND URA.USR_ID  = MRN.USR_ID
                   AND MRN.MNU_SEQ = BTM.MNU_SEQ
                   AND TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMMDD') BETWEEN AR.APL_ST_DT AND AR.APL_ED_DT
                   AND TO_CHAR(CURRENT_TIMESTAMP, 'YYYYMMDD') BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                   AND AR.US_YN = 'Y'
                   AND URA.USR_ID =  #{user_id ,jdbcType=VARCHAR}
              ) T1
           ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN
	</select>

    <!-- 관리자 메뉴 조회 (2018-02-10) -->
    <select id="selectTsysAdmMnuListAll" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuListAll][관리자 메뉴 조회 (2018-02-10)][TANINE]*/
          SELECT  T1.* 
                , ROW_NUMBER() OVER(PARTITION BY T1.MIDD_MNU_SEQ  ORDER BY T1.TOP_SRT_SQN, T1.MIDD_SRT_SQN, T1.BTM_SRT_SQN) AS SRT_SQN
           FROM
               (
                 SELECT DISTINCT
                        TOP.MNU_SEQ     AS TOP_MNU_SEQ
                      , TOP.MNU_NM      AS TOP_MNU_NM
                      , TOP.SRT_SQN 	AS TOP_SRT_SQN
                      , MIDD.MNU_SEQ 	AS MIDD_mnu_seq
                      , MIDD.MNU_NM 	AS MIDD_MNU_NM
                      , MIDD.SRT_SQN 	AS MIDD_SRT_SQN
                      , BTM.MNU_SEQ
                      , BTM.MNU_NM
                      , BTM.SRT_SQN     AS BTM_SRT_SQN
                      , BTM.MNU_TP_CD
                      , CASE WHEN MIDD.URL IS NULL THEN BTM.URL END AS URL
                      , CASE WHEN TOP.US_YN = 'Y'
                             THEN CASE WHEN MIDD.US_YN = 'Y' THEN BTM.US_YN ELSE MIDD.US_YN END
                             ELSE TOP.US_YN
                        END AS US_YN
                   FROM TSYS_ADM_MNU TOP 		-- 최상단 메뉴
                      , TSYS_ADM_MNU MIDD		-- 중단 메뉴
                      , TSYS_ADM_MNU BTM		-- 하단 메뉴
                      , TSYS_USR_ROL_RTNN URA
                      , TSYS_ADM_MNU_ROL_RTNN MRA
                      , TSYS_USR_ROL UR
                  WHERE TOP.MNU_SEQ = MIDD.UP_MNU_SEQ
                    AND MIDD.MNU_SEQ = BTM.UP_MNU_SEQ
                    AND URA.ROL_ID = MRA.ROL_ID
                    AND URA.ROL_ID = UR.ROL_ID
                    AND TOP.FRNT_YN = 'N'
                    AND MIDD.FRNT_YN = 'N'
                    AND BTM.FRNT_YN = 'N'
                    AND UR.US_YN = 'Y'
         ) T1
         <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(url)">AND V.URL = #{url ,jdbcType=VARCHAR}</if>
         ORDER BY TOP_SRT_SQN, MIDD_SRT_SQN, BTM_SRT_SQN
    </select>

    <!-- 사용자 아이디별 메뉴 권한 -->
    <select id="selectTsysAdmMnuAuthByUserId" parameterType="ParameterMap" resultType="DataMap">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuAuthByUserId][사용자 아이디별 메뉴 권한][TANINE]*/
        SELECT CASE WHEN SUM(CHK.STD_COUNT) > 0 AND SUM(CHK.EXISTS_COUNT) = 0 THEN 'F' ELSE 'S' END AUTH_RST
             , CHK.MNU_SEQ
          FROM (
                SELECT COUNT(AM.MNU_SEQ) STD_COUNT
                     , COUNT(U.MNU_SEQ)  EXISTS_COUNT
                     , MAX(AM.MNU_SEQ)   MNU_SEQ
                  FROM TSYS_ADM_MNU AM
                  LEFT OUTER JOIN ( SELECT MRA.MNU_SEQ
                                      FROM TSYS_USR_ROL_RTNN URA
                                         , TSYS_USR_ROL UR
                                         , TSYS_ADM_MNU_ROL_RTNN MRA
                                     WHERE UR.ROL_ID = URA.ROL_ID
                                       AND URA.USR_ID = #{usr_id, jdbcType=VARCHAR}
                                       AND UR.ROL_ID = MRA.ROL_ID
                                       AND SYSDATE BETWEEN URA.APL_ST_DT AND URA.APL_ED_DT
                                       AND SYSDATE BETWEEN UR.APL_ST_DT AND UR.APL_ED_DT
                                       AND UR.US_YN = 'Y') U ON U.MNU_SEQ = AM.MNU_SEQ
                  WHERE AM.URL = #{url, jdbcType=VARCHAR}
                AND  AM.US_YN = 'Y'
        ) CHK
        GROUP BY CHK.MNU_SEQ
    </select>

    <!-- FRONT 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuFrntList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuFrntList][FRONT 메뉴 리스트 조회][TANINE]*/
        SELECT MNU_SEQ
             , UP_MNU_SEQ
             , MNU_NM
             , MNU_DPTH
             , URL
             , MNU_TP_CD
             , MNU_YN
             , US_YN
             , LST_MNU_YN
             , SRT_SQN
             , MNUL_ID
             , MNU_DESC
             , CRTR_ID
             , CRT_DTTM
             , UPDR_ID
             , UPD_DTTM
          FROM TSYS_ADM_MNU
         WHERE FRNT_YN = 'Y'
         ORDER BY MNU_DESC
    </select>

    <select id="selectTsysAdmMnuDownGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
     /*[TsysAdmMnu.xml][selectTsysAdmMnuDownGridList][][TANINE]*/
     <include refid="Common.getPaging-Prefix" />
         SELECT A.MNU_SEQ
              , A.UP_MNU_SEQ
              , A.MNU_NM
              , A.MNU_DPTH
              , A.US_YN
              , A.MNU_YN
              -- , A.SRT_SQN
              , A.MNU_TP_CD
              , A.PGM_ID
              , SI.PGM_NM
              , SI.TRG_URL
           FROM TSYS_ADM_MNU A
           LEFT OUTER JOIN  TSYS_PGM_INF  SI
             ON A.PGM_ID = SI.PGM_ID
          WHERE 1=1
            AND A.UP_MNU_SEQ = #{up_mnu_seq ,jdbcType=NUMERIC}
            AND A.MNU_SEQ IS NOT NULL
           	<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_seq)" >
           	AND A.SIT_SEQ = #{sit_seq}
           	</if>
          <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
          ORDER BY ${sidx} ${sord}
          </if>
          <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">
          ORDER BY A.SRT_SQN
          </if>
        <include refid="Common.getPaging-Suffix" />
    </select>

    <!-- BO 메뉴 개별 조회 -->
    <select id="selectTsysAdmMnuTgt" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuTgt][BO 메뉴 개별 조회][TANINE]*/
        SELECT MNU_SEQ
             , UP_MNU_SEQ
             , MNU_NM
          FROM TSYS_ADM_MNU
         WHERE FRNT_YN = 'N'
           AND US_YN = 'Y'
           AND URL = #{url ,jdbcType=VARCHAR}
           AND SIT_SEQ = #{sit_seq, jdbcType=NUMERIC}
    </select>

    <!-- FO 메뉴 URL 조회 -->
    <select id="selectTsysAdmMnuURL" parameterType="String" resultType="TsysAdmMnu">
    /*[TsysAdmMnu.xml][selectURL][FO 메뉴 URL 조회][TANINE]*/
        SELECT URL FROM TSYS_ADM_MNU WHERE URL LIKE #{url, 	jdbcType=VARCHAR}
    </select>

    <!-- 메뉴아이디 얻기 -->
    <select id="selectTsysAdmMnuId" parameterType="ParameterMap" resultType="java.lang.String">
        /*[TsysAdmMnu.xml][selectMenuId][메뉴아이디 얻기][TANINE]*/
       SELECT NVL(MAX(MNU_SEQ), 0)
        FROM TSYS_ADM_MNU
       WHERE US_YN = 'Y'
          AND URL = #{url ,jdbcType=VARCHAR}
	</select>
	
	<!-- 공통 > breadCrumb(홈 > 시스템관리 > 코드관리 > 코드관리) 메뉴 조회 -->
    <select id="selectTsysAdmMnuBreadCrumb" parameterType="ParameterMap" resultType="TsysAdmMnu">
		/*[TsysAdmMnu.xml][selectTsysAdmMnuBreadCrumb][breadCrumb 메뉴조회][TANINE]*/
		SELECT TOP.MNU_SEQ  AS TOP_MNU_SEQ
             , TOP.MNU_NM   AS TOP_MNU_NM
             , MIDD.MNU_SEQ AS MIDD_MNU_SEQ
             , MIDD.MNU_NM  AS MIDD_MNU_NM
             , BTM.MNU_SEQ  AS MNU_SEQ
             , BTM.MNU_NM   AS MNU_NM
          FROM TSYS_ADM_MNU TOP
         INNER JOIN TSYS_ADM_MNU MIDD 
            ON (TOP.MNU_SEQ = MIDD.UP_MNU_SEQ)
         INNER JOIN TSYS_ADM_MNU BTM 
            ON (MIDD.MNU_SEQ = BTM.UP_MNU_SEQ)
         WHERE BTM.MNU_SEQ = #{mnu_seq ,jdbcType=NUMERIC}
    </select>
    
    <!-- 권한의 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuRolRtnnGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuRolRtnnGridList][권한의 메뉴 리스트 조회][TANINE]*/
		SELECT RM.ROL_ID						AS ROL_ID
			 , RM.MNU_SEQ						AS MNU_SEQ
			 , MN.MNU_NM						AS MNU_NM
			 , MN.UP_MNU_SEQ					AS MIDD_MNU_SEQ
			 , (SELECT MNU_NM
				  FROM TSYS_ADM_MNU
				 WHERE MNU_SEQ = MN.UP_MNU_SEQ)	AS MIDD_MNU_NM
			 , MN.SRT_SQN						AS SRT_SQN
			 , MN.PGM_ID						AS PGM_ID
             , MN.SIT_SEQ						AS SIT_SEQ
		  FROM TSYS_ADM_MNU				MN
			 , TSYS_ADM_MNU_ROL_RTNN	RM
		 WHERE MN.MNU_SEQ	= RM.MNU_SEQ
		   AND MN.MNU_DPTH	= 3
		   AND MN.FRNT_YN	= 'N'
		   AND MN.MNU_YN	= 'Y'
		   AND MN.US_YN		= 'Y'
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rol_id)">
		   AND RM.ROL_ID	= #{rol_id, jdbcType=VARCHAR}
		</if>
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_seq)">
		   AND MN.SIT_SEQ	= #{sit_seq, jdbcType=NUMERIC}
		</if>
		 ORDER BY MIDD_MNU_SEQ, SRT_SQN
	</select>
	
	<!-- 권한 메뉴의 버튼 리스트 조회 -->
    <select id="selectTsysAdmMnuRolRtnnBtnGridList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuRolRtnnBtnGridList][권한 메뉴의 버튼 리스트 조회][TANINE]*/
		  SELECT (SELECT TO_CHAR(WM_CONCAT (PB.PGM_BTN_CD)) 
				    FROM TSYS_PGM_BTN_INF PB
				   WHERE PB.PGM_ID = T1.PGM_ID)				       AS PGM_BTN
			   , ( SELECT TO_CHAR(WM_CONCAT (RB.PGM_BTN_CD)) 
				     FROM TSYS_ORG_BTN_ROL_RTNN RB
				    WHERE RB.PGM_ID = T1.PGM_ID
				      AND RB.ROL_ID =  #{rol_id, jdbcType=VARCHAR}
				      AND RB.MNU_SEQ = #{mnu_seq, jdbcType=VARCHAR})  AS ROL_BTN
		    FROM TSYS_PGM_INF T1
		   WHERE T1.PGM_ID = #{pgm_id, jdbcType=VARCHAR}
		     AND T1.US_YN = 'Y'
	</select>
    
    <!-- 권한 추가를 위한 메뉴 리스트 조회 -->
    <select id="selectTsysAdmMnuBtnList" parameterType="ParameterMap" resultType="TsysAdmMnu">
        /*[TsysAdmMnu.xml][selectTsysAdmMnuBtnList][권한 추가 대상 메뉴/버튼 조회][TANINE]*/
        <include refid="Common.getPaging-Prefix" />
       		SELECT MN.MNU_SEQ						AS MNU_SEQ
				 , MN.MNU_NM						AS MNU_NM
				 , MN.UP_MNU_SEQ					AS MIDD_MNU_SEQ
				 , (SELECT MNU_NM
					  FROM TSYS_ADM_MNU
					 WHERE MNU_SEQ = MN.UP_MNU_SEQ)	AS MIDD_MNU_NM
				 , MN.SRT_SQN						AS SRT_SQN
				 , MN.PGM_ID						AS PGM_ID
				 , (SELECT WM_CONCAT(PGM_BTN_CD)
					  FROM TSYS_PGM_BTN_INF PB
					 WHERE PB.PGM_ID = MN.PGM_ID
					   AND US_YN = 'Y')				AS PGM_BTN
				 , MN.MNU_DPTH						AS MNU_DPTH
				 , MN.SIT_SEQ						AS SIT_SEQ
			  FROM TSYS_ADM_MNU	MN
			 WHERE 1 = 1
			   AND MN.FRNT_YN	= 'N'
			   AND MN.MNU_YN	= 'Y'
			   AND MN.US_YN		= 'Y'
			   AND MN.PGM_ID IS NOT NULL
			   AND MN.MNU_SEQ IS NOT NULL
			   AND MN.MNU_DPTH = 3
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_seq)">
			   AND MN.SIT_SEQ = #{sit_seq, jdbcType=NUMERIC}
			</if>   
	 		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(mnu_nm)">
			   AND MN.MNU_NM LIKE '%' || #{mnu_nm, jdbcType=VARCHAR} || '%'
			</if>
       
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
			 ORDER BY ${sidx} ${sord}
			</if>
			<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">
			 ORDER BY MIDD_MNU_SEQ, SRT_SQN
			</if>
		<include refid="Common.getPaging-Suffix" />
	</select>
</mapper>