<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">

<!-- 시스템 로그 조회 XML -->
<mapper namespace="TsysLogInf">

	<!-- 메뉴접속 이력 그리드 리스트  -->
    <select id="selectTsysLogInfList" parameterType="ParameterMap" resultType="TsysLogInf">
		/*[TsysLogInf.xml][selectSysLogInfoList][메뉴접속이력리스트][TANINE]*/
		SELECT P.*
		  FROM (SELECT V.*
					<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rows)">
					 , CEIL(V.ID / #{rows, jdbcType=NUMERIC})        AS PAGE
					 , @ROW_NUMBER                                   AS TOTAL_COUNT
					 , CEIL(@ROW_NUMBER / #{rows, jdbcType=NUMERIC}) AS TOTAL_PAGE
					</if>
				  FROM (SELECT LI.*
							 , (@ROW_NUMBER := @ROW_NUMBER + 1 ) ID 
						  FROM (SELECT LI.USR_ID                     AS USR_ID       /* 사용자ID */
									 , LI.IP_ADDR                    AS IP_ADDR      /* IP주소 */
									 , LI.RQST_URL                   AS RQST_URL     /* 요청URL */
									 , LI.RQST_PARA                  AS RQST_PARA    /* 요청파라미터 */
									 , LI.CRT_DTTM                   AS CRT_DTTM     /* 생성일시 */
									 , IFNULL(AL.LGN_ID, '-')        AS LGN_ID       /* 로그인ID */
									 , IFNULL(AM.MNU_NM, '')         AS MNU_NM       /* 메뉴명 */
								  FROM TSYS_LOG_INF        LI
							 LEFT JOIN TMBR_ADM_LGN_INF    AL
									ON LI.USR_ID = AL.USR_ID
							 LEFT JOIN TSYS_ADM_MNU        AM
									ON LI.RQST_URL = AM.URL
								 WHERE 1 = 1
								   AND LI.RQST_URL = AM.URL
						 	<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rqst_url)">
						 	       AND LI.RQST_URL LIKE CONCAT('%', #{rqst_url, jdbcType=VARCHAR}, '%')
					        </if>
						 	<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_st_dt)">
						 		   AND DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') <![CDATA[>=]]> #{sch_st_dt, jdbcType=VARCHAR}
					        </if>
					        <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_ed_dt)">
					        	   AND DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') <![CDATA[<=]]> #{sch_ed_dt, jdbcType=VARCHAR}
					        </if>
					        <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(ip_addr)">
					        	   AND LI.IP_ADDR LIKE CONCAT('%', #{ip_addr, jdbcType=VARCHAR}, '%')
					        </if>
								 ORDER BY CRT_DTTM DESC
							   ) LI
						     , (SELECT @ROW_NUMBER := 0) RN
					   ) V
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
				 ORDER BY V.${sidx} ${sord}
				</if>
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">     
				 ORDER BY V.CRT_DTTM DESC
				</if>           
			   ) P
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(page)">
		 WHERE P.PAGE = #{page, jdbcType=NUMERIC}
		</if>
	</select>
    
	<!-- 프론트메뉴접속이력 그리드리스트  -->
	<select id="selectTsysLogInfFrntList" parameterType="ParameterMap" resultType="TsysLogInf">
		/*[TsysLogInf.xml][selectTsysLogInfFrntList][프론트메뉴접속이력리스트][TANINE]*/
		SELECT P.*
		  FROM (SELECT V.*
					<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rows)">
					 , CEIL(V.ID / #{rows, jdbcType=NUMERIC})               AS PAGE
					 , @ROW_NUMBER                                          AS TOTAL_COUNT
					 , CEIL(@ROW_NUMBER / #{rows, jdbcType=NUMERIC})        AS TOTAL_PAGE
					</if>
				  FROM (SELECT LI.*
                             , (@ROW_NUMBER := @ROW_NUMBER + 1 ) ID 
						  FROM (SELECT LI.IP_ADDR                           AS IP_ADDR      /* IP주소 */
									 , LI.RQST_URL                          AS RQST_URL     /* 요청RUL */
									 , DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') AS F_CRT_DTTM   /* 생성일자 */
									 , IFNULL(AM.MNU_NM, '')                AS MNU_NM       /* 메뉴명 */
									 , AM.SIT_SEQ                           AS SIT_SEQ      /* 사이트번호 */
									 , (SELECT SI.SIT_NM 
										  FROM TDSP_SIT_INF SI
										 WHERE SI.SIT_SEQ = AM.SIT_SEQ)     AS SIT_NM       /* 사이트명 */
									 , COUNT(1)                             AS CNT          /* 접속수 */
								  FROM TSYS_LOG_INF    LI
							 LEFT JOIN TSYS_ADM_MNU    AM
									ON LI.RQST_URL = AM.URL 
								 WHERE 1 = 1
								   AND AM.FRNT_YN = 'Y'
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rqst_url)">
								   AND LI.RQST_URL LIKE CONCAT('%',#{rqst_url ,jdbcType=VARCHAR},'%')
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_st_dt)">
								   AND DATE_FORMAT(LI.CRT_DTTM,'%Y-%m-%d') <![CDATA[>=]]> #{sch_st_dt, jdbcType=VARCHAR}
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_ed_dt)">
								   AND DATE_FORMAT(LI.CRT_DTTM,'%Y-%m-%d') <![CDATA[<=]]> #{sch_ed_dt, jdbcType=VARCHAR}
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_seq)">
								   AND LI.SIT_SEQ = #{sit_seq ,jdbcType=VARCHAR}
								</if>
								 GROUP BY LI.IP_ADDR, LI.RQST_URL, DATE_FORMAT(LI.CRT_DTTM, '%Y%m%d'), IFNULL(AM.MNU_NM, ''), AM.SIT_NO
								 ORDER BY F_CRT_DTTM DESC
							   ) LI
							 , (SELECT @ROW_NUMBER := 0) RN
                       ) V
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
				 ORDER BY V.${sidx} ${sord}
				</if>
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">     
				 ORDER BY V.F_CRT_DTTM DESC
				</if>          
			   ) P
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(page)">
		 WHERE P.PAGE = #{page ,jdbcType=NUMERIC}
		</if>
    </select>
    
    
	<!-- 프론트 메뉴접속이력 그리드리스트  -->
	<select id="selectTsysLogInfFrntMnuList" parameterType="ParameterMap" resultType="TsysLogInf">
		/*[TsysLogInf.xml][selectTsysLogInfFrntMnuList][프론트메뉴접속이력리스트][TANINE]*/
		SELECT P.*
		  FROM (SELECT V.*
					<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(rows)">
					 , CEIL(V.ID / #{rows, jdbcType=NUMERIC})         AS PAGE
					 , @ROW_NUMBER                                    AS TOTAL_COUNT
					 , CEIL(@ROW_NUMBER / #{rows, jdbcType=NUMERIC})  AS TOTAL_PAGE
					</if>
				  FROM (SELECT Y.*
							 , (@ROW_NUMBER := @ROW_NUMBER + 1) ID 
						  FROM (SELECT LI.USR_ID                            AS USR_ID       /* 사용자ID */
									 , LI.IP_ADDR                           AS IP_ADDR      /* IP주소 */
									 , LI.RQST_URL                          AS RQST_URL     /* 요청URL */
									 , LI.RQST_PARA                         AS RQST_PARA    /* 요청파라미터 */
									 , LI.CRT_DTTM                          AS CRT_DTTM     /* 생성일시 */
									 , DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') AS F_CRT_DTTM   /* 등록일 */
									 , IFNULL(UI.LGN_ID, '-')               AS LGN_ID       /* 로그인ID */
									 , IFNULL(AM.MNU_NM, '')                AS MNU_NM       /* 메뉴명 */
									 , AM.SIT_SEQ                           AS SIT_SEQ      /* 사이트번호 */
								  FROM TSYS_LOG_INF    LI
							 LEFT JOIN TMBR_USR_INF    UI
							 		ON LI.USR_ID = UI.USR_ID
							INNER JOIN TSYS_ADM_MNU    AM
									ON LI.MNU_SEQ = AM.MNU_SEQ
								 WHERE 1 = 1
								   AND AM.US_YN   = 'Y'
								   AND AM.FRNT_YN = 'Y'
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sit_no)">
								   AND AM.SIT_NO = #{sit_no, jdbcType=VARCHAR}
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_rqst_url)">
								   AND LI.RQST_URL LIKE CONCAT('%', #{sch_rqst_url, jdbcType=VARCHAR}, '%')
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_st_dt)">
								   AND DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') <![CDATA[>=]]> #{sch_st_dt, jdbcType=VARCHAR}
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_ed_dt)">
								   AND DATE_FORMAT(LI.CRT_DTTM, '%Y-%m-%d') <![CDATA[<=]]> #{sch_ed_dt, jdbcType=VARCHAR}
								</if>
								<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sch_ip_addr)">
								   AND LI.IP_ADDR LIKE CONCAT('%', #{sch_ip_addr, jdbcType=VARCHAR}, '%')
								</if>
								 ORDER BY CRT_DTTM DESC
							   ) Y
							 , (SELECT @ROW_NUMBER := 0) RN
					   )V
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(sidx)">
				 ORDER BY V.${sidx} ${sord}
				</if>
				<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isEmpty(sidx)">     
				 ORDER BY V.CRT_DTTM DESC
				</if>           
			   ) P
		<if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(page)">
		 WHERE P.PAGE = #{page ,jdbcType=NUMERIC}
		</if>
    </select>
    
</mapper>