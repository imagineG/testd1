<?xml version="1.0" encoding="UTF-8"?>
<!DOCTYPE mapper PUBLIC "-//mybatis.org//DTD Mapper 3.0//EN" "http://mybatis.org/dtd/mybatis-3-mapper.dtd">
<mapper namespace="TmbrUsrInfTrx">
	
	<!-- 마이페이지 : 사용자 수정 -->
    <update id="updateTmbrUsrInfMyPageInfo" parameterType="ParameterMap">
        /*[BaseTmbrUsrInf.xml][updateTmbrUsrInfMyPageInfo][마이페이지 : 사용자 수정][TANINE]*/
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
             , USR_NM = #{usr_nm, jdbcType=VARCHAR}
             , PWD_QST = #{pwd_qst, jdbcType=VARCHAR}
             , PWD_ASW = #{pwd_asw, jdbcType=VARCHAR}
             <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(eml_rcv_yn)">
             , EML_RCV_YN = #{eml_rcv_yn, jdbcType=VARCHAR}
             </if>
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR} 
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
    
    <!-- 사용자 비밀번호 변경 -->
    <update id="updateTmbrUsrInfPwChg" parameterType="TmbrUsrInf">
        /*[BaseTmbrUsrInf.xml][updateTmbrUsrInfPwChg][사용자 비밀번호 변경][TANINE]*/
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
             , PWD_UPD_DTTM = DATE_FORMAT(SYSDATE(), '%Y%m%d%H%i%s')
             , LGN_PWD = #{lgn_pwd, jdbcType=VARCHAR}
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR} 
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
    
    <!-- 사용자 비밀번호 변경 (ParameterMap)-->
    <update id="updateTmbrUsrInfPwChgMap" parameterType="ParameterMap">
    	/*[BaseTmbrUsrInf.xml][updateTmbrUsrInfPwChgMap][사용자 비밀번호 변경(ParameterMap)][TANINE]*/
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
             , PWD_UPD_DTTM = DATE_FORMAT(SYSDATE(), '%Y%m%d%H%i%s')
             , LGN_PWD = #{lgn_pwd, jdbcType=VARCHAR}
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR} 
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
    
    <!-- 최종로그인 정보 변경 -->
    <update id="updateTmbrUsrInfLastLogin" parameterType="TmbrUsrLgnInf">
        /*[BaseTmbrUsrInf.xml][updateTmbrUsrInfLastLogin][최종로그인 정보 변경][TANINE]*/
        UPDATE TMBR_USR_INF
           SET LAST_LOGIN_DATE = SYSDATE()
             , IP_ADDR = #{ip_addr, jdbcType=VARCHAR}
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR} 
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
    
    <!-- 사용자 비밀번호 변경일 변경 (ParameterMap)-->
    <update id="updateTmbrUsrInfPwChgDate" parameterType="ParameterMap">
        /*[BaseTmbrUsrInf.xml][updateTmbrUsrInfPwChgDate][사용자 비밀번호 변경일 변경(ParameterMap)][TANINE]*/
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
             , PWD_UPD_DTTM = DATE_FORMAT(SYSDATE(), '%Y%m%d%H%i%s')
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR} 
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
	
	<!-- 패스워드 실패횟수 초기화 -->
    <update id="updateTmbrUsrInfInitUserPwInfo" parameterType="TmbrUsrInf">
        /* [TmbrUsrInfTrx.xml][updateTmbrUsrInfInitUserPwInfo][패스워드 실패횟수 초기화][TANINE] */
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
		     , PWD_FAIL_CNT = 0
		     , PWD_FAIL_DTTM = NULL
             <if test="@kr.co.ta9.pandora3.common.util.TextUtil@isNotEmpty(actv_yn)">
             , ACTV_YN = #{actv_yn, jdbcType=VARCHAR}
             </if>
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR}
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
	
	<!-- 사용자 패스워드 정보 초기화 -->
    <update id="updateTmbrUsrInfInitPwd" parameterType="TmbrUsrInf">
    	/* [TmbrUsrInfTrx.xml][updateInitUserPwInfoAll][사용자 패스워드 정보 초기화][TANINE] */
        UPDATE TMBR_USR_INF
           SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
             , LGN_PWD = #{lgn_pwd, jdbcType=VARCHAR}
             , PWD_FAIL_CNT = 0
		     , PWD_FAIL_DTTM = NULL
             , ACTV_YN = 'Y'
             , PWD_UPD_DTTM = #{pwd_upd_dttm, jdbcType=VARCHAR}
         WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR}
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
    </update>
    
	<!-- 회원 상태 코드 탈퇴 처리 -->
	<update id="updateTmbrUsrInfUsrSsCd" parameterType="TmbrUsrInf">
		/* [TmbrUsrInfTrx.xml][updateTmbrUsrInfMemberLeaveStsByAdmin][회원 상태 코드 탈퇴 처리][TANINE] */
		UPDATE TMBR_USR_INF
		   SET UPDR_ID = #{updr_id, jdbcType=VARCHAR}
		     , UPD_DTTM = SYSDATE()
		 	 , USR_SS_CD = '2'
		 WHERE LGN_ID = #{lgn_id, jdbcType=VARCHAR}
           AND USR_ID = #{usr_id, jdbcType=VARCHAR}
	</update>
	
</mapper>