package kr.co.ta9.pandora3.pmbr.dao.base;

import org.springframework.stereotype.Repository;

import kr.co.ta9.pandora3.app.repository.BaseDao;
import kr.co.ta9.pandora3.common.exception.PrimaryKeyNotSettedException;
import kr.co.ta9.pandora3.pcommon.dto.TmbrClu;

/**
 * BaseTmbrCluDaoTrx - Transactional BASE DAO(Base Data Access Object) class for table
 * [TMBR_CLU].
 *
 * <pre>
 *  Do not modify this file
 *  Copyright &amp;copy 2004 by Pionnet, Inc. All rights reserved.
 * </pre>
 *
 * @since 2019. 02. 16
 */
@Repository
public class BaseTmbrCluDaoTrx extends BaseDao {

	/**
	 * @param tmbrClu TmbrClu
	 * @return boolean
	 */
	public boolean isPrimaryKeyValid(TmbrClu tmbrClu) {
		boolean result = true;
		if (tmbrClu == null) {
			result = false;
		}

		if (
			tmbrClu.getObj_clu_seq()== null
			) {
			result = false;
		}
		return result;
	}

	/**
	 * @param tmbrClu TmbrClu
	 * @throws Exception
	 */
	public  int insert(TmbrClu tmbrClu) throws Exception {
		return getSqlSession().insert("TmbrCluTrx.insert", tmbrClu);
	}

	/**
	 * @param tmbrClus TmbrClu[]
	 * @throws Exception
	 */
	public int insertMany(TmbrClu[] tmbrClus) throws Exception {
		int count = 0;
		if (tmbrClus != null) {
			for (TmbrClu tmbrClu : tmbrClus) {
				count += insert(tmbrClu);
			}
		}
		return count;
	}

	/**
	 * @param tmbrClu TmbrClu
	 * @throws Exception
	 */
	public  int update(TmbrClu tmbrClu) throws Exception {
		if(!isPrimaryKeyValid(tmbrClu)) {
			throw new PrimaryKeyNotSettedException("instance of TmbrClu can't be null or Primary key is not set");
		}
		return getSqlSession().update("TmbrCluTrx.update", tmbrClu);
	}

	/**
	 * @param tmbrClus TmbrClu[]
	 * @throws Exception
	 */
	public  int updateMany(TmbrClu[] tmbrClus) throws Exception {
		 int count = 0;
		if (tmbrClus != null) {
			for (TmbrClu tmbrClu : tmbrClus) {
				count += update(tmbrClu);
			}
		}
		return count;
	}

	/**
	 * @param tmbrClu TmbrClu
	 * @throws Exception
	 */
	public  int delete(TmbrClu tmbrClu)throws Exception {
		if(!isPrimaryKeyValid(tmbrClu)) {
			throw new PrimaryKeyNotSettedException("instance of TmbrClu can't be null or Primary key is not set");
		}
		return getSqlSession().delete("TmbrCluTrx.delete", tmbrClu);
	}

	/**
	 * @param tmbrClus TmbrClu[]
	 * @throws Exception
	 */
	public int deleteMany( TmbrClu[] tmbrClus) throws Exception {
		int count = 0;
		if (tmbrClus != null) {
			for (TmbrClu tmbrClu : tmbrClus) {
				count += delete(tmbrClu);
			}
		}
		return count;
	}

}