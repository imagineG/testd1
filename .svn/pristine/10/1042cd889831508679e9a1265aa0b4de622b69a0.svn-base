package kr.co.ta9.pandora3.psys.manager;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.lang.StringUtils;
import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ta9.pandora3.app.entry.UserInfo;
import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.pcommon.dto.TsysAdmMnuBtnRtnn;
import kr.co.ta9.pandora3.pcommon.dto.TsysAdmMnuRtnn;
import kr.co.ta9.pandora3.psys.dao.TsysAdmMnuBtnRtnnDao;
import kr.co.ta9.pandora3.psys.dao.TsysAdmMnuBtnRtnnDaoTrx;
import kr.co.ta9.pandora3.psys.dao.TsysAdmMnuRtnnDao;
import kr.co.ta9.pandora3.psys.dao.TsysAdmMnuRtnnDaoTrx;

/**
 * <pre>
 * 1. 클래스명 : Psys1029Mgr
 * 2. 설명 : 시스템회원 메뉴 권한관리 서비스
 * 3. 작성일 : 2019-03-12
 * 4. 작성자 : TANINE
 * </pre>
 */

@Service
public class Psys1029Mgr {
	
	@Autowired
	private TsysAdmMnuRtnnDao tsysAdmMnuRtnnDao;
	@Autowired
	private TsysAdmMnuRtnnDaoTrx tsysAdmMnuRtnnDaoTrx;
	
	@Autowired
	private TsysAdmMnuBtnRtnnDao tsysAdmMnuBtnRtnnDao;
	@Autowired
	private TsysAdmMnuBtnRtnnDaoTrx tsysAdmMnuBtnRtnnDaoTrx;
	
	/**
	 * 관리자메뉴할당 그리드 조회
	 * @param  parameterMap
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getTsysAdmMnuRtnnGrdList(ParameterMap parameterMap) throws Exception {
		return tsysAdmMnuRtnnDao.getTsysAdmMnuRtnnGrdList(parameterMap);
	}
	
	/**
	 * 관리자메뉴할당 여부 조회
	 * @param  parameterMap
	 * @return String
	 * @throws Exception
	 */
	public String getTsysAdmMnuRtnnYn(ParameterMap parameterMap) throws Exception {
		return tsysAdmMnuRtnnDao.getTsysAdmMnuRtnnYn(parameterMap);
	}
	
	/**
	 * 관리자메뉴할당 정보 삭제
	 * @param  parameterMap
	 * @throws Exception
	 */
	public void deleteTsysAdmMnuRtnnList(ParameterMap parameterMap) throws Exception {
		List<TsysAdmMnuRtnn> deleteList = new ArrayList<TsysAdmMnuRtnn>();
		parameterMap.populates(TsysAdmMnuRtnn.class, null, null, deleteList);
		TsysAdmMnuRtnn[] delete = deleteList.toArray(new TsysAdmMnuRtnn[deleteList.size()]);
		
		// 맵핑 메뉴 삭제
		int delCnt = tsysAdmMnuRtnnDaoTrx.deleteMany(delete);
		
		// 맵핑 메뉴 삭제 시 권리자메뉴버튼할당 테이블 데이터도 삭제
		if(delCnt > 0) {
			List<TsysAdmMnuBtnRtnn> deleteBtnList = new ArrayList<TsysAdmMnuBtnRtnn>();
			parameterMap.populates(TsysAdmMnuBtnRtnn.class, null, null, deleteBtnList);
			TsysAdmMnuBtnRtnn[] deleteBtn = deleteBtnList.toArray(new TsysAdmMnuBtnRtnn[deleteBtnList.size()]);
			for(int i = 0; i < deleteBtn.length; i++) {
				tsysAdmMnuBtnRtnnDaoTrx.deleteTsysAdmMnuBtnRtnnInf(deleteBtn[i]);
			}
		}
	}
	
	/**
	 * 관리자메뉴할당 정보 저장
	 * @param  parameterMap
	 * @throws Exception
	 */
	public void insertTsysAdmMnuRtnnList(ParameterMap parameterMap) throws Exception {
		List<TsysAdmMnuRtnn> insertList = new ArrayList<TsysAdmMnuRtnn>();
		parameterMap.populates(TsysAdmMnuRtnn.class, insertList, null, null);	
		TsysAdmMnuRtnn[] insert = insertList.toArray(new TsysAdmMnuRtnn[insertList.size()]);
		
		// 맵핑 메뉴 저장
		tsysAdmMnuRtnnDaoTrx.insertMany(insert);
	}
	
	/**
	 * 권리자메뉴버튼할당 정보 조회
	 * @param  parameterMap
	 * @return JSONObject
	 * @throws Exception
	 */
	public JSONObject getTsysAdmMnuBtnRtnnInf(ParameterMap parameterMap) throws Exception {
		return tsysAdmMnuBtnRtnnDao.getTsysAdmMnuBtnRtnnInf(parameterMap);
	}
	
	/**
	 * 권리자메뉴버튼할당 정보 삭제 후 저장
	 * @param  parameterMap
	 * @throws Exception
	 */
	public void saveTsysAdmMnuBtnRtnnList(ParameterMap parameterMap) throws Exception {
		// 권리자메뉴버튼할당 정보 삭제
		TsysAdmMnuBtnRtnn tsysAdmMnuBtnRtnn = new TsysAdmMnuBtnRtnn();
		tsysAdmMnuBtnRtnn.setUsr_id(parameterMap.getValue("usr_id"));
		tsysAdmMnuBtnRtnn.setMnu_seq(parameterMap.getInt("mnu_seq"));
		tsysAdmMnuBtnRtnn.setPgm_id(parameterMap.getValue("pgm_id"));
		UserInfo userInfo = parameterMap.getUserInfo();
		String user_id = userInfo.getUser_id();
		tsysAdmMnuBtnRtnn.setUpdr_id(user_id);
		tsysAdmMnuBtnRtnn.setCrtr_id(user_id);
		tsysAdmMnuBtnRtnnDaoTrx.deleteTsysAdmMnuBtnRtnnInf(tsysAdmMnuBtnRtnn);
		
		// 권리자메뉴버튼할당 정보 저장
		String mpp_btn_inf = StringUtils.isNotEmpty(parameterMap.getValue("mpp_btn_inf")) ? parameterMap.getValue("mpp_btn_inf") : "";
		if(!"".equals(mpp_btn_inf)) {
			String[] mpp_btn_inf_arr = mpp_btn_inf.split(",");
			for(int i = 0; i < mpp_btn_inf_arr.length; i++) {
				tsysAdmMnuBtnRtnn.setPgm_btn_cd(String.valueOf(mpp_btn_inf_arr[i]));
				tsysAdmMnuBtnRtnnDaoTrx.insert(tsysAdmMnuBtnRtnn);
			}
		}
	}
	
}
