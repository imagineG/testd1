package kr.co.ta9.pandora3.front.story.manager;

import java.lang.reflect.Field;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.json.simple.JSONObject;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import kr.co.ta9.pandora3.app.servlet.ParameterMap;
import kr.co.ta9.pandora3.common.util.BeanUtil;
import kr.co.ta9.pandora3.front.notice.dao.NoticeDao;
import kr.co.ta9.pandora3.front.story.dao.StoryDao;
import kr.co.ta9.pandora3.pcommon.dto.TbbsDocInf;

/**
* <pre>
* 1. 패키지명 : kr.co.ta9.pandora3.front.story.manager
* 2. 타입명 : class
* 3. 작성일 : 2018-04-24
* 4. 설명 : Story 매니저
* </pre>
*/
@Service
public class StoryManager {
	
	@Autowired
	NoticeDao noticeDao;
	
	@Autowired
	StoryDao storyDao;
	
	/**
	 * BOARD : 게시글 목록 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getDocumentList(ParameterMap parameterMap) throws Exception {

		List<TbbsDocInf> dataList = noticeDao.getDocumentList(parameterMap);
		List<Map<String,Object>> mapList = convertObjectToMapList(dataList);		
		JSONObject json = new JSONObject();		
		json.put("mapList", mapList);
		return json;
	}
	
	/**
	 * BOARD : 게시글 이미지 목록 조회
	 * @param parameterMap
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("unchecked")
	public JSONObject getStoryImgList(ParameterMap parameterMap) throws Exception {

		List<TbbsDocInf> dataList = storyDao.getStoryImgList(parameterMap);
		List<Map<String,Object>> mapList = convertObjectToMapList(dataList);		
		JSONObject json = new JSONObject();		
		json.put("mapList", mapList);
		return json;
	}
	
	/**
	 * List to List Map
	 * @param list
	 * @return
	 * @throws Exception
	 */
	@SuppressWarnings("rawtypes")
	public List<Map<String,Object>> convertObjectToMapList(List list) throws Exception {
		List<Map<String,Object>> results = new ArrayList<Map<String,Object>>();
		if (list == null) {
			return results;
		}
		for (Object object : list) {
			results.add(convertObjectToMap(object));
		}
		return results;
	}
	
	/**
	 * Object to Map
	 * @param object
	 * @return
	 * @throws Exception
	 */
	private Map<String,Object> convertObjectToMap(Object object) throws Exception {
		Map<String,Object> result = new HashMap<String,Object>();
		List<Field> fields = BeanUtil.getAllDeclaredFields(object.getClass());
		for (Field field : fields) {
			field.setAccessible(true);
			result.put(field.getName(), field.get(object));
		}		
		return result;
	}
}
